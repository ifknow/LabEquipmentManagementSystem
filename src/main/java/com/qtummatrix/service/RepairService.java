package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.RepairLog;
import com.qtummatrix.entity.RepairLogXO;

/**
 * create by Gongshiyong  2020-01-23 12:01
 */
public interface RepairService {

    public PageInfo selectAllRepairLog(RepairLogXO repairLog, Integer pageNo, Integer pageSize);
}
