package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.User;
import com.qtummatrix.entity.UserXO;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-21 16:42
 */
public interface UserService {

    /**
     * 查询所有的普通用户信息
     *
     * @param pageNo
     * @param pageSize
     * @param stunumber
     * @param phone
     * @param classs
     * @param realname
     * @return
     */
    public PageInfo selectAllUserList(Integer pageNo, Integer pageSize, String stunumber, String phone, String classs,
                                      String realname, Integer userId);

    /**
     * 查询所有的管理员信息
     *
     * @param pageNo
     * @param pageSize
     * @param stunumber
     * @param phone
     * @param classs
     * @param realname
     * @return
     */
    public PageInfo selectAllAdminList(Integer pageNo, Integer pageSize, String stunumber, String phone, String classs,
                                       String realname, Integer userId);

    /**
     * 根据主键查询所有的用户信息
     *
     * @param id
     * @return
     */
    public List<UserXO> selectAllUserListByPrimaryKey(Integer id);

    /**
     * 保存编辑的信息
     *
     * @param userXO
     * @return
     */
    public int updateByPrimaryKeySelective(UserXO userXO);

    /**
     * 保存用户信息
     *
     * @param userXO
     * @return
     */
    public int insertSelective(UserXO userXO);

    /**
     * 验证手机号
     *
     * @param phone
     * @return
     */
    public int validatePhone(String phone);

    /**
     * 根据id删除用户信息
     *
     * @param id
     * @return
     */
    public int deleteByPrimaryKey(Integer id);

    /**
     * 修改密码
     *
     * @param id
     * @param oldPassword
     * @param password
     * @return
     */
    public int changePassword(Integer id, String oldPassword, String password);

    /**
     * 注册
     *
     * @param user
     * @return
     */
    public int registerUser(User user);

    /**
     * 管理员审核用户
     *
     * @param id
     * @return
     */
    public int checkUser(Integer id);

    /**
     * 重置管理员密码
     *
     * @param id
     * @return
     */
    public int resetPassWord(Integer id);
}
