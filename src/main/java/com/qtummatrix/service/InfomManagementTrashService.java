package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;

public interface InfomManagementTrashService {

    public PageInfo getAllTrashNotice(Integer pageNo, Integer pageSize, String name);

    public int cancelDeleteNoticeById(Integer id);
}
