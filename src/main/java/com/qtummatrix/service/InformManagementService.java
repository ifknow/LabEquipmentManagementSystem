package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.Notice;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-21 10:49
 */
public interface InformManagementService {

    public PageInfo getAllNotice(Integer pageNo, Integer pageSize, String name);

    public List<Notice> getAllNoticeById(Integer id);

    public int updateByPrimaryKeySelective(Notice notice);

    public int deleteByPrimaryKey(Integer id);

    public int insertSelective(Notice notice);
}
