package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.LabAppointmentMapper;
import com.qtummatrix.entity.Laboratory;
import com.qtummatrix.service.LabAppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-22 13:18
 */
@Service
public class LabAppointmentServiceImpl implements LabAppointmentService {

    @Autowired
    private LabAppointmentMapper labAppointmentMapper;

    private Map<String, Object> map = new HashMap<String, Object>();

    @Override
    public PageInfo selectAllLabInfo(Integer pageNo, Integer pageSize, String name, String fzr, Integer isYy) {
        PageHelper.startPage(pageNo, pageSize);
        map.put("name", name);
        map.put("fzr", fzr);
        //已预约
        if (isYy == 2){
            map.put("isYy", 1);
        //未预约
        }else if (isYy == 1){
            map.put("isYy", 0);
        }else{
            map.put("isYy", null);
        }
        List<Laboratory> laboratories = labAppointmentMapper.selectAllLabInfo(map);
        PageInfo pageInfo = new PageInfo<>(laboratories);
        return pageInfo;
    }

    @Override
    public int appointment(Integer id, Integer userId) {
        map.put("id", id);
        map.put("userId", userId);
        //查询实验室信息
        List<Laboratory> laboratories = labAppointmentMapper.selectLabInfoById(id);
        Integer isYy = laboratories.get(0).getIsYy();
        int i = 0;
        if (isYy == 1) {
            Integer userId1 = laboratories.get(0).getUserId();
            if (!userId1.equals(userId)) {
                //表示操作失败，不是当前用户
                return -1;
            }
            //已预约就取消预约
            i = labAppointmentMapper.cancelappointment(map);
            map.put("time", new Date());
            i = labAppointmentMapper.insertLabAppointInfo1(map);
        } else {
            //未预约进行预约
            i = labAppointmentMapper.appointment(map);
            map.put("time", new Date());
            i = labAppointmentMapper.insertLabAppointInfo(map);
        }
        return i;
    }
}
