package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.UserMapper;
import com.qtummatrix.entity.User;
import com.qtummatrix.entity.UserXO;
import com.qtummatrix.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-21 16:42
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    private Map<String, Object> map = new HashMap<>();

    public PageInfo selectAllUserList(Integer pageNo, Integer pageSize, String stunumber, String phone, String classs,
                                      String realname, Integer userId) {
        Map<String, Object> info = new HashMap<String, Object>();
        PageInfo pageInfo = null;
        //首先判断用户是否是管理员
        List<UserXO> userXOS = userMapper.selectAllUserListByPrimaryKey(userId);
        String rolename = userXOS.get(0).getRolename();
        if (rolename.equals("管理员")) {
            PageHelper.startPage(pageNo, pageSize);
            info.put("stunumber", stunumber);
            info.put("phone", phone);
            info.put("classs", classs);
            info.put("realname", realname);
            List<UserXO> userList = userMapper.selectAllUserList(info);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (UserXO user : userList) {
                Date time = user.getTime();
                String formattime = format.format(time);
                user.setTimeStr(formattime);
            }
            pageInfo = new PageInfo<>(userList);
        } else if (rolename.equals("超级管理员")) {
            PageHelper.startPage(pageNo, pageSize);
            info.put("stunumber", stunumber);
            info.put("phone", phone);
            info.put("classs", classs);
            info.put("realname", realname);
            List<UserXO> userList = userMapper.selectAllUserList(info);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (UserXO user : userList) {
                Date time = user.getTime();
                String formattime = format.format(time);
                user.setTimeStr(formattime);
            }
            pageInfo = new PageInfo<>(userList);
        } else {
            PageHelper.startPage(pageNo, pageSize);
            info.put("stunumber", stunumber);
            info.put("phone", phone);
            info.put("classs", classs);
            info.put("realname", realname);
            info.put("userId", userId);
            List<UserXO> userList = userMapper.selectAllUserList(info);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (UserXO user : userList) {
                Date time = user.getTime();
                String formattime = format.format(time);
                user.setTimeStr(formattime);
            }
            pageInfo = new PageInfo<>(userList);
        }
        return pageInfo;
    }

    @Override
    public PageInfo selectAllAdminList(Integer pageNo, Integer pageSize, String stunumber, String phone, String classs, String realname, Integer userId) {
        Map<String, Object> info = new HashMap<String, Object>();
        PageInfo pageInfo = null;
        //首先判断用户是否是管理员
        List<UserXO> userXOS = userMapper.selectAllUserListByPrimaryKey(userId);
        String rolename = userXOS.get(0).getRolename();
        if (rolename.equals("管理员")) {
            PageHelper.startPage(pageNo, pageSize);
            info.put("stunumber", stunumber);
            info.put("phone", phone);
            info.put("classs", classs);
            info.put("realname", realname);
            List<UserXO> userList = userMapper.selectAllAdminList(info);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (UserXO user : userList) {
                Date time = user.getTime();
                String formattime = format.format(time);
                user.setTimeStr(formattime);
            }
            pageInfo = new PageInfo<>(userList);
        } else if (rolename.equals("超级管理员")) {
            PageHelper.startPage(pageNo, pageSize);
            info.put("stunumber", stunumber);
            info.put("phone", phone);
            info.put("classs", classs);
            info.put("realname", realname);
            List<UserXO> userList = userMapper.selectAllAdminList(info);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (UserXO user : userList) {
                Date time = user.getTime();
                String formattime = format.format(time);
                user.setTimeStr(formattime);
            }
            pageInfo = new PageInfo<>(userList);
        } else {
            PageHelper.startPage(pageNo, pageSize);
            info.put("stunumber", stunumber);
            info.put("phone", phone);
            info.put("classs", classs);
            info.put("realname", realname);
            info.put("userId", userId);
            List<UserXO> userList = userMapper.selectAllUserList(info);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (UserXO user : userList) {
                Date time = user.getTime();
                String formattime = format.format(time);
                user.setTimeStr(formattime);
            }
            pageInfo = new PageInfo<>(userList);
        }
        return pageInfo;
    }

    @Override
    public List<UserXO> selectAllUserListByPrimaryKey(Integer id) {
        return userMapper.selectAllUserListByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(UserXO userXO) {
        return userMapper.updateByPrimaryKeySelective(userXO);
    }

    @Override
    public int insertSelective(UserXO userXO) {
        return userMapper.insertSelective(userXO);
    }

    @Override
    public int validatePhone(String phone) {
        List<UserXO> userXOS = userMapper.validatePhone(phone);
        if (userXOS.size() == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int changePassword(Integer id, String oldPassword, String password) {
        List<User> user = userMapper.selectUserPassword(id);
        String password1 = user.get(0).getPassword();
        if (password1.equals(oldPassword)) {
            //修改密码
            User user1 = new User();
            user1.setId(id);
            user1.setPassword(password);
            int i = userMapper.updateByPrimaryKey(user1);
            if (i > 0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    @Override
    public int registerUser(User user) {
        return userMapper.registerUser(user);
    }

    @Override
    public int checkUser(Integer id) {
        return userMapper.checkUser(id);
    }

    @Override
    public int resetPassWord(Integer id) {
        return userMapper.resetPassWord(id);
    }
}
