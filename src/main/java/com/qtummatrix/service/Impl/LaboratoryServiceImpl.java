package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.LaboratoryMapper;
import com.qtummatrix.entity.Laboratory;
import com.qtummatrix.service.LaboratoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-22 10:59
 */
@Service
public class LaboratoryServiceImpl implements LaboratoryService {

    @Autowired
    private LaboratoryMapper laboratoryMapper;

    private Map<String, Object> map = new HashMap<String, Object>();

    @Override
    public PageInfo selectAllLabInfo(Integer pageNo, Integer pageSize, String name, String fzr) {
        PageHelper.startPage(pageNo, pageSize);
        map.put("name", name);
        map.put("fzr", fzr);
        List<Laboratory> laboratories = laboratoryMapper.selectAllLabInfo(map);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Laboratory laboratory : laboratories) {
            Date time = laboratory.getTime();
            String formattime = format.format(time);
            laboratory.setTimeStr(formattime);
        }
        PageInfo pageInfo = new PageInfo<>(laboratories);
        return pageInfo;
    }

    @Override
    public List<Laboratory> selectAllLabInfoNoPage() {
        return laboratoryMapper.selectAllLabInfoNoPage();
    }

    @Override
    public List<Laboratory> selectByPrimaryKey(Integer id) {
        return laboratoryMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Laboratory laboratory) {
        return laboratoryMapper.updateByPrimaryKeySelective(laboratory);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return laboratoryMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(Laboratory laboratory) {
        return laboratoryMapper.insertSelective(laboratory);
    }
}
