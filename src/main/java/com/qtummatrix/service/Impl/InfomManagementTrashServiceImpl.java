package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.NoticeMapper;
import com.qtummatrix.entity.Notice;
import com.qtummatrix.service.InfomManagementTrashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class InfomManagementTrashServiceImpl implements InfomManagementTrashService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public PageInfo getAllTrashNotice(Integer pageNo, Integer pageSize, String name) {
        PageHelper.startPage(pageNo, pageSize);
        List<Notice> allTrashNotice = noticeMapper.getAllTrashNotice(name);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Notice notice : allTrashNotice) {
            Date time = notice.getTime();
            String formattime = format.format(time);
            notice.setTimeStr(formattime);
        }
        PageInfo pageInfo = new PageInfo<>(allTrashNotice);
        return pageInfo;
    }

    @Override
    public int cancelDeleteNoticeById(Integer id) {
        return noticeMapper.cancelDeleteNoticeById(id);
    }
}
