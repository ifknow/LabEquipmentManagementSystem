package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.EquipmentlogMapper;
import com.qtummatrix.entity.EquipmentLogXO;
import com.qtummatrix.service.EquipmentLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * create by Gongshiyong  2020-01-23 17:44
 */
@Service
public class EquipmentLogServiceImpl implements EquipmentLogService {

    @Autowired
    private EquipmentlogMapper equipmentlogMapper;

    @Override
    public PageInfo selectAllEquipmentLog(Integer pageNo, Integer pageSize, String xh, String JyUserName) {
        PageHelper.startPage(pageNo, pageSize);
        List<EquipmentLogXO> equipmentLogXOS = equipmentlogMapper.selectAllEquipmentLog(xh, JyUserName);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (EquipmentLogXO equipmentLogXO : equipmentLogXOS) {
            Date time = equipmentLogXO.getTime();
            String timeStr = format.format(time);
            equipmentLogXO.setTimeStr(timeStr);
            if (equipmentLogXO.getEndTime() != null) {
                Date endTime = equipmentLogXO.getEndTime();
                String endTimeStr = format.format(endTime);
                equipmentLogXO.setEndTimeStr(endTimeStr);
            }
        }
        PageInfo<EquipmentLogXO> pageInfo = new PageInfo<>(equipmentLogXOS);
        return pageInfo;
    }
}
