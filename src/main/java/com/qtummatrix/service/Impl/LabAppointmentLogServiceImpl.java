package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.LaboratoryLogMapper;
import com.qtummatrix.entity.Laboratory;
import com.qtummatrix.entity.LaboratoryLog;
import com.qtummatrix.service.LabAppointmentLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LabAppointmentLogServiceImpl implements LabAppointmentLogService {

    @Autowired
    private LaboratoryLogMapper laboratoryLogMapper;

    private Map<String, Object> map = new HashMap<String, Object>();

    @Override
    public PageInfo selectAllLabLogInfo(Integer pageNo, Integer pageSize, String name, String fzr) {
        PageHelper.startPage(pageNo, pageSize);
        map.put("name", name);
        map.put("fzr", fzr);
        List<LaboratoryLog> laboratories = laboratoryLogMapper.selectAllLabLogInfo(map);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (LaboratoryLog laboratory : laboratories) {
            Date time = laboratory.getTime();
            String timestr = format.format(time);
            laboratory.setTimestr(timestr);
            if (laboratory.getEndtime() != null) {
                Date endtime = laboratory.getEndtime();
                String endtimestr = format.format(endtime);
                laboratory.setEndtimestr(endtimestr);
            }
        }
        PageInfo pageInfo = new PageInfo<>(laboratories);
        return pageInfo;
    }
}
