package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.RepairLogMapper;
import com.qtummatrix.entity.RepairLogXO;
import com.qtummatrix.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * create by Gongshiyong  2020-01-23 12:01
 */
@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    private RepairLogMapper repairLogMapper;

    @Override
    public PageInfo selectAllRepairLog(RepairLogXO repairLog, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<RepairLogXO> repairLogXOS = repairLogMapper.selectAllRepairLog(repairLog);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (RepairLogXO repairLogXO : repairLogXOS) {
            Date time = repairLogXO.getBxTime();
            String formattime = format.format(time);
            repairLogXO.setBxTimeStr(formattime);
            if (repairLogXO.getEndTime() != null) {
                Date time1 = repairLogXO.getEndTime();
                String formattime1 = format.format(time1);
                repairLogXO.setEndTimeStr(formattime1);
            }
        }
        PageInfo pageInfo = new PageInfo<>(repairLogXOS);
        return pageInfo;
    }
}
