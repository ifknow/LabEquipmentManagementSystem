package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.NoticeMapper;
import com.qtummatrix.entity.Notice;
import com.qtummatrix.service.InformManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * create by Gongshiyong  2020-01-21 10:50
 */
@Service
public class InformManagementServiceImpl implements InformManagementService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public PageInfo getAllNotice(Integer pageNo, Integer pageSize, String name) {
        PageHelper.startPage(pageNo, pageSize);
        List<Notice> allNotice = noticeMapper.getAllNotice(name);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Notice notice : allNotice) {
            Date time = notice.getTime();
            String formattime = format.format(time);
            notice.setTimeStr(formattime);
        }
        PageInfo pageInfo = new PageInfo<>(allNotice);
        return pageInfo;
    }

    @Override
    public List<Notice> getAllNoticeById(Integer id) {
        return noticeMapper.getAllNoticeById(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Notice notice) {
        return noticeMapper.updateByPrimaryKeySelective(notice);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return noticeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(Notice notice) {
        return noticeMapper.insertSelective(notice);
    }
}
