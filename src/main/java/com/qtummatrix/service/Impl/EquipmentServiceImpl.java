package com.qtummatrix.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qtummatrix.dao.EquipmentMapper;
import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.RepairLog;
import com.qtummatrix.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * create by Gongshiyong  2020-01-22 16:06
 */
@Service
public class EquipmentServiceImpl implements EquipmentService {

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Override
    public PageInfo selectAllEquipmentInfo(Equipment equipment, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<Equipment> equipment1 = equipmentMapper.selectAllEquipmentInfo(equipment);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (Equipment equipment2 : equipment1) {
            Date time = equipment2.getFwTime();
            String formattime = format.format(time);
            equipment2.setTimeStr(formattime);
            Date time1 = equipment2.getTime();
            String format1 = format.format(time1);
            equipment2.setTimeStrFw(format1);
        }
        PageInfo pageInfo = new PageInfo<>(equipment1);
        return pageInfo;
    }

    @Override
    public List<Equipment> selectByPrimaryKey(Integer id) {
        return equipmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Equipment equipment) {
        return equipmentMapper.updateByPrimaryKeySelective(equipment);
    }

    @Override
    public int insertSelective(Equipment equipment) {
        return equipmentMapper.insertSelective(equipment);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return equipmentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Collection selectEquipmentInfoExcel(String ids) {
        // 将用逗号分隔的字符串进行分隔存进数组中
        String[] id = ids.split(",");
        Collection collection = equipmentMapper.selectEquipmentInfoExcel(id);
        return collection;
    }

    @Override
    public int uploadImage(Integer id, String imagePath) {
        return equipmentMapper.uploadImage(id, imagePath);
    }

    @Override
    public int cancelBx(Integer id) {
        int i1 = equipmentMapper.cancelBx(id);
        int i = equipmentMapper.cancelBxInfo(id);
        return 1;
    }

    @Override
    public int addBxInfo(RepairLog repairLog) {
        Date date = new Date();
        repairLog.setBxtime(date);
        //修改设备的isBx为1
        int i = equipmentMapper.updateIsBx(repairLog.getId());
        //添加报修记录
        int i1 = equipmentMapper.insertBxInfo(repairLog);
        List<RepairLog> repairLogs = equipmentMapper.selectRepairLog();
        Equipment equipment = new Equipment();
        equipment.setBxLogId(repairLogs.get(0).getId());
        equipment.setId(repairLog.getId());
        int i2 = equipmentMapper.updateByPrimaryKeySelective(equipment);
        if (i == 1 && i1 == 1 && i2 == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int batchDeleteData(String ids) {
        // 将用逗号分隔的字符串进行分隔存进数组中
        String[] id = ids.split(",");
        return equipmentMapper.batchDeleteData(id);
    }
}
