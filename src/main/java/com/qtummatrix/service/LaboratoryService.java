package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.Laboratory;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-22 10:59
 */
public interface LaboratoryService {

    /**
     * 查询所有的实验室
     *
     * @param pageNo
     * @param pageSize
     * @param name
     * @param fzr
     * @return
     */
    public PageInfo selectAllLabInfo(Integer pageNo, Integer pageSize, String name, String fzr);

    /**
     * 查询所有的实验室信息
     *
     * @return
     */
    public List<Laboratory> selectAllLabInfoNoPage();

    /**
     * 根据主键查询实验室信息
     *
     * @param id
     * @return
     */
    public List<Laboratory> selectByPrimaryKey(Integer id);

    /**
     * 保存编辑后的实验室信息
     *
     * @param laboratory
     * @return
     */
    public int updateByPrimaryKeySelective(Laboratory laboratory);

    /**
     * 根据主键删除实验室信息
     *
     * @param id
     * @return
     */
    public int deleteByPrimaryKey(Integer id);

    /**
     * 新建实验室
     *
     * @param laboratory
     * @return
     */
    public int insertSelective(Laboratory laboratory);
}
