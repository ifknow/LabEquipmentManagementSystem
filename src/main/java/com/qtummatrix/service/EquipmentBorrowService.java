package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;

/**
 * create by Gongshiyong  2020-01-23 14:03
 */
public interface EquipmentBorrowService {

    /**
     * 查询所有的设备借用详情
     *
     * @param pageNo
     * @param pageSize
     * @param xh
     * @param zzs
     * @return
     */
    public PageInfo selectAllEquipmentInfo(Integer pageNo, Integer pageSize, String xh, String zzs);

    /**
     * 借用或归还设备
     *
     * @param id
     * @param userId
     * @return
     */
    public int equipmentBackAndBorrow(Integer id, Integer userId);
}
