package com.qtummatrix.service;

import com.qtummatrix.entity.User;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-20 11:16
 */
public interface UserLoginService {

    /**
     * 登录验证
     *
     * @param userAccount  手机号
     * @param userPassword 密码
     * @return
     */
    public List<User> validateLogin(String userAccount, String userPassword);

}
