package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;

public interface LabAppointmentLogService {

    public PageInfo selectAllLabLogInfo(Integer pageNo, Integer pageSize, String name, String fzr);
}
