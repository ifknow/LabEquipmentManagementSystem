package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;

/**
 * create by Gongshiyong  2020-01-23 17:44
 */
public interface EquipmentLogService {

    /**
     * 查询所有设备日志
     * @param pageNo
     * @param pageSize
     * @return
     */
    public PageInfo selectAllEquipmentLog(Integer pageNo, Integer pageSize,String xh, String JyUserName);
}
