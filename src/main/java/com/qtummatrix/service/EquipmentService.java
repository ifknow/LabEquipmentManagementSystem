package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.RepairLog;

import java.util.Collection;
import java.util.List;

/**
 * create by Gongshiyong  2020-01-22 16:06
 */
public interface EquipmentService {

    /**
     * 查询所有的设备技术参数
     *
     * @param equipment
     * @param pageNo
     * @param pageSize
     * @return
     */
    public PageInfo selectAllEquipmentInfo(Equipment equipment, Integer pageNo, Integer pageSize);

    /**
     * 根据主键查询设备信息
     *
     * @param id
     * @return
     */
    public List<Equipment> selectByPrimaryKey(Integer id);

    /**
     * 保存修改后的信息
     *
     * @param equipment
     * @return
     */
    public int updateByPrimaryKeySelective(Equipment equipment);

    /**
     * 新建设备信息
     *
     * @param equipment
     * @return
     */
    public int insertSelective(Equipment equipment);

    /**
     * 删除
     *
     * @param id
     * @return
     */
    public int deleteByPrimaryKey(Integer id);

    /**
     * 获取要导出的数据
     *
     * @param ids
     * @return
     */
    public Collection selectEquipmentInfoExcel(String ids);

    /**
     * 上传图片
     *
     * @param id
     * @param imagePath
     * @return
     */
    public int uploadImage(Integer id, String imagePath);

    /**
     * 取消保修设备
     *
     * @param id
     * @return
     */
    public int cancelBx(Integer id);

    /**
     * 添加报修设备信息
     *
     * @param repairLog
     * @return
     */
    public int addBxInfo(RepairLog repairLog);

    /**
     * 批量删除设备
     *
     * @param ids
     * @return
     */
    public int batchDeleteData(String ids);
}
