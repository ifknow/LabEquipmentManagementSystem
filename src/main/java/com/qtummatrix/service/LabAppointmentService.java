package com.qtummatrix.service;

import com.github.pagehelper.PageInfo;

/**
 * create by Gongshiyong  2020-01-22 13:17
 */
public interface LabAppointmentService {

    /**
     * 查询所有的实验室预约信息
     *
     * @param pageNo
     * @param pageSize
     * @param name
     * @param fzr
     * @return
     */
    public PageInfo selectAllLabInfo(Integer pageNo, Integer pageSize, String name, String fzr, Integer isYy);

    /**
     * 预约实验室
     *
     * @param id
     * @param userId
     * @return
     */
    public int appointment(Integer id, Integer userId);

}
