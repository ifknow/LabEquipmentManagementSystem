package com.qtummatrix.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request1 = (HttpServletRequest) request;
        HttpServletResponse response1 = (HttpServletResponse) response;
        HttpSession session = request1.getSession();
        if (session.getAttribute("LOGIN_USER") == null) {
            // 没有登录
            response1.sendRedirect(request1.getContextPath() + "/login/login");
        } else {
            // 已经登录，继续请求下一级资源（继续访问）
            chain.doFilter(request1, response1);
        }
    }

    @Override
    public void destroy() {

    }
}
