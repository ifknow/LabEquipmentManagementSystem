package com.qtummatrix.utils;

import cn.hutool.poi.excel.ExcelWriter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collection;

/**
 * create by Gongshiyong  2020-01-07 19:10
 */
public class ExcelUtil {

    /**
     * 将所选的商品信息导出为Excel（用hutool的poi）
     *
     * @param response
     * @param collection 商品集合
     * @param filename   文件名
     * @throws IOException
     */
    public static void getExcel(HttpServletResponse response, Collection collection, String filename) throws IOException {
        ExcelWriter writer = cn.hutool.poi.excel.ExcelUtil.getWriter(true);
        //自定义标题别名
        writer.addHeaderAlias("id", "ID");
        writer.addHeaderAlias("bz", "备注");
        writer.addHeaderAlias("isYy", "是否预约");
        writer.addHeaderAlias("jyId", "预约记录Id");

        writer.addHeaderAlias("xh", "设备型号");
        writer.addHeaderAlias("zzs", "制造商");
        writer.addHeaderAlias("jg", "设备价格");
        writer.addHeaderAlias("time", "服务开始时间");
        writer.addHeaderAlias("fwTime", "服务结束时间");
        writer.addHeaderAlias("bxLogId", "报修日志Id");
        writer.addHeaderAlias("isBx", "是否报修");
        writer.addHeaderAlias("isDelete", "是否删除");
        writer.addHeaderAlias("isFw", "服务");
        writer.addHeaderAlias("iyId", "是否预约");
        writer.addHeaderAlias("jyUserId", "借用人Id");
        writer.addHeaderAlias("laboratoryId", "所属实验室Id");
        writer.addHeaderAlias("photo", "图片");
        writer.addHeaderAlias("sbxlh", "设备序列号");


        //一次性写出内容，使用默认样式
        writer.write(collection);
        response.reset();
        response.setContentType("application/octet-stream;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
        writer.flush(response.getOutputStream());
        writer.close();
    }
}
