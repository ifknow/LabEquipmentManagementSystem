package com.qtummatrix.dao;

import com.qtummatrix.entity.RepairLog;
import com.qtummatrix.entity.RepairLogXO;

import java.util.List;

public interface RepairLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RepairLog record);

    int insertSelective(RepairLog record);

    RepairLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RepairLog record);

    int updateByPrimaryKey(RepairLog record);

    /**
     * 查询所有的报修日志信息
     *
     * @param repairLogxo
     * @return
     */
    public List<RepairLogXO> selectAllRepairLog(RepairLogXO repairLogxo);
}