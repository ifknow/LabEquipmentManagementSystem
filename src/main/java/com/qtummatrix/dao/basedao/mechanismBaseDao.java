package com.qtummatrix.dao.basedao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class mechanismBaseDao extends SqlSessionDaoSupport{

	@Resource
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}
	
	public <T> List<T> selectList(String sqlId, Object param) {
		return this.getSqlSession().selectList(sqlId, param);
	}

	public <T> List<T> selectList(String sqlId) {
		return this.getSqlSession().selectList(sqlId);
	}
	
	public int delete(String sqlid,Object param) {
		return this.getSqlSession().delete(sqlid,param);
	}
	
	public <T> T selectOne(String sqlid,Object param){
		return this.getSqlSession().selectOne(sqlid, param);
	}
	
	public int insert(String sqlid,Object param) {
		return this.getSqlSession().insert(sqlid, param);
	}
}
