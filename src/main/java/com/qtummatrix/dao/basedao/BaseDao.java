package com.qtummatrix.dao.basedao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * create by Gongshiyong 2019年12月16日
 */
@Repository
public class BaseDao extends SqlSessionDaoSupport {
	
	@Resource
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}

	public <T> List<T> selectList(String sqlId, Object param) {
		return this.getSqlSession().selectList(sqlId, param);
	}

	public <T> List<T> selectList(String sqlId) {
		return this.getSqlSession().selectList(sqlId);
	}

	public <T> T selectOne(String sqlId, Object param) {
		return this.getSqlSession().selectOne(sqlId, param);
	}

	public <T> T selectOne(String sqlId) {
		return this.getSqlSession().selectOne(sqlId);
	}

	public int delete(String sqlId, Object param) {
		return this.getSqlSession().delete(sqlId, param);
	}

	public int delete(String sqlId) {
		return this.getSqlSession().delete(sqlId);
	}

	public int update(String sqlId, Object param) {
		return this.getSqlSession().update(sqlId, param);
	}

	public int update(String sqlId) {
		return this.getSqlSession().update(sqlId);
	}

	public int insert(String sqlId, Object param) {
		return this.getSqlSession().insert(sqlId, param);
	}

	public int insert(String sqlId) {
		return this.getSqlSession().insert(sqlId);
	}

	/**
	 * 分页
	 * 
	 * @param <T>
	 * @param pageNo   当前页码
	 * @param pageSize 每页的记录数
	 * @param sqlId    SQL语句的ID
	 * @param param    参数
	 * @return
	 */
	public <T> PageInfo<T> selectPage(int pageNo, int pageSize, String sqlId, Object param) {
		PageHelper.startPage(pageNo, pageSize, true);
		List<T> result = this.getSqlSession().selectList(sqlId, param);
		return new PageInfo<T>(result);
	}

}
