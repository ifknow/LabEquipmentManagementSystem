package com.qtummatrix.dao;

import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.RepairLog;

import java.util.Collection;
import java.util.List;

public interface EquipmentMapper {

    /**
     * 删除设备
     *
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 新建设备信息
     *
     * @param record
     * @return
     */
    int insertSelective(Equipment record);

    /**
     * 根据主键查询设备信息
     *
     * @param id
     * @return
     */
    public List<Equipment> selectByPrimaryKey(Integer id);

    /**
     * 查询所有设备参数信息
     *
     * @param equipment
     * @return
     */
    public List<Equipment> selectAllEquipmentInfo(Equipment equipment);

    /**
     * 保存修改后的信息
     *
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Equipment record);

    /**
     * 获取要导出为excel的数据
     *
     * @param id
     * @return
     */
    public Collection selectEquipmentInfoExcel(String[] id);

    /**
     * 上传图片
     *
     * @param id
     * @param imagePath
     * @return
     */
    public int uploadImage(Integer id, String imagePath);

    /**
     * 取消保修
     *
     * @param id
     * @return
     */
    public int cancelBx(Integer id);

    public int cancelBxInfo(Integer id);

    /**
     * 添加报修信息
     *
     * @param id
     * @return
     */
    public int updateIsBx(Integer id);

    public int insertBxInfo(RepairLog repairLog);

    public List<RepairLog> selectRepairLog();

    /**
     * 批量删除设备
     *
     * @param ids
     * @return
     */
    public int batchDeleteData(String[] ids);
}