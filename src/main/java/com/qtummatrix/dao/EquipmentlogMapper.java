package com.qtummatrix.dao;

import com.qtummatrix.entity.EquipmentLogXO;

import java.util.List;

public interface EquipmentlogMapper {

    /**
     * 查询所有设备日志
     *
     * @return
     */
    public List<EquipmentLogXO> selectAllEquipmentLog(String xh, String JyUserName);


}