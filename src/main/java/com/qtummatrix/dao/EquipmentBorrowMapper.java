package com.qtummatrix.dao;

import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.EquipmentBorrow;
import com.qtummatrix.entity.Equipmentlog;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-23 14:02
 */
public interface EquipmentBorrowMapper {

    /**
     * 查询所有的设备借用信息
     *
     * @param xh
     * @param zzs
     * @return
     */
    public List<EquipmentBorrow> selectAllEquipmentInfo(String xh, String zzs);

    /**
     * 查询设备的借出情况
     *
     * @param id
     * @return
     */
    public List<Equipment> selectEquipmentInfoById(Integer id);

    /**
     * 返还设备
     *
     * @param map
     * @return
     */
    public int backEquipment(Map<String, Object> map);

    /**
     * 借用设备
     *
     * @param map
     * @return
     */
    public int borrowEquipment(Map<String, Object> map);

    /**
     * 添加设备借出信息
     *
     * @param id
     * @param userId
     * @param date
     * @return
     */
    public int addEquipmentBorrowLog(Integer id, Integer userId, Date date);

    /**
     * 查询返还记录
     *
     * @param userId
     * @param equipmentId
     * @return
     */
    public List<Equipmentlog> selectLogEquipment(Integer userId, Integer equipmentId);

    /**
     * 添加设备返还信息
     *
     * @param id
     * @param date
     * @return
     */
    public int addEquipmentReturnLog(Integer id, Date date);
}
