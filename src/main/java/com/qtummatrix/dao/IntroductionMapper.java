package com.qtummatrix.dao;

import com.qtummatrix.entity.Introduction;

public interface IntroductionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Introduction record);

    int insertSelective(Introduction record);

    Introduction selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Introduction record);

    int updateByPrimaryKey(Introduction record);
}