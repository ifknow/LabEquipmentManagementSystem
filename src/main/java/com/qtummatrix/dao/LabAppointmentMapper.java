package com.qtummatrix.dao;

import com.qtummatrix.entity.Laboratory;

import java.util.List;
import java.util.Map;

public interface LabAppointmentMapper {

    //查询所有的实验室
    public List<Laboratory> selectAllLabInfo(Map map);

    //预约实验室
    public int appointment(Map map);

    //取消实验室预约
    public int cancelappointment(Map map);

    //查询实验室信息ById
    public List<Laboratory> selectLabInfoById(Integer id);

    public int insertLabAppointInfo(Map<String, Object> map);

    public int insertLabAppointInfo1(Map<String, Object> map);
}