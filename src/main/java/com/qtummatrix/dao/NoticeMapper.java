package com.qtummatrix.dao;

import com.qtummatrix.entity.Notice;

import java.util.List;

public interface NoticeMapper {

    //对公告信息进行软删除
    public int deleteByPrimaryKey(Integer id);

    //保存新建的公告信息
    public int insertSelective(Notice record);

    //查询所有的公告信息
    public List<Notice> getAllNotice(String name);

    //根据公告id获取公告信息
    public List<Notice> getAllNoticeById(Integer id);

    //编辑公告信息
    public int updateByPrimaryKeySelective(Notice record);

    //获取回收站中的通知信息
    public List<Notice> getAllTrashNotice(String name);

    //撤回删除公告
    public int cancelDeleteNoticeById(Integer id);
}