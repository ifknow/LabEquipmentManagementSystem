package com.qtummatrix.dao;

import com.qtummatrix.entity.Laboratory;

import java.util.List;
import java.util.Map;

public interface LaboratoryMapper {

    //删除实验室信息
    public int deleteByPrimaryKey(Integer id);

    //新建实验室信息
    public int insertSelective(Laboratory record);

    //查询所有的实验室
    public List<Laboratory> selectAllLabInfo(Map map);

    //根据主键查询实验室信息
    public List<Laboratory> selectByPrimaryKey(Integer id);

    //保存编辑后的实验室信息
    public int updateByPrimaryKeySelective(Laboratory record);

    public int updateByPrimaryKey(Laboratory record);

    //查询所有的实验室信息
    public List<Laboratory> selectAllLabInfoNoPage();
}