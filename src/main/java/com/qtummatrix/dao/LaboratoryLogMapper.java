package com.qtummatrix.dao;

import com.qtummatrix.entity.Laboratory;
import com.qtummatrix.entity.LaboratoryLog;

import java.util.List;
import java.util.Map;

public interface LaboratoryLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LaboratoryLog record);

    int insertSelective(LaboratoryLog record);

    LaboratoryLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LaboratoryLog record);

    int updateByPrimaryKey(LaboratoryLog record);

    /**
     * 查询所有的实验室预约记录信息
     *
     * @param map
     * @return
     */
    List<LaboratoryLog> selectAllLabLogInfo(Map<String, Object> map);
}