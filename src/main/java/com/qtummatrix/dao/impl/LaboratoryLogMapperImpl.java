package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.LaboratoryLogMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.LaboratoryLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class LaboratoryLogMapperImpl implements LaboratoryLogMapper {

    @Autowired
    private BaseDao baseDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(LaboratoryLog record) {
        return 0;
    }

    @Override
    public int insertSelective(LaboratoryLog record) {
        return 0;
    }

    @Override
    public LaboratoryLog selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(LaboratoryLog record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(LaboratoryLog record) {
        return 0;
    }

    @Override
    public List<LaboratoryLog> selectAllLabLogInfo(Map<String, Object> map) {
        return baseDao.selectList("com.qtummatrix.dao.LaboratoryLogMapper.selectByPrimaryKey", map);
    }
}
