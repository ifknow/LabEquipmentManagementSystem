package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.EquipmentMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.EquipmentVo;
import com.qtummatrix.entity.RepairLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * create by Gongshiyong  2020-01-22 16:04
 */
@Repository
public class EquipmentMapperImpl implements EquipmentMapper {

    private Map<String, Object> map = new HashMap<>();

    @Autowired
    private BaseDao baseDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return baseDao.update("com.qtummatrix.dao.EquipmentMapper.deleteByPrimaryKey", id);
    }

    @Override
    public int insertSelective(Equipment record) {
        return baseDao.insert("com.qtummatrix.dao.EquipmentMapper.insertSelective", record);
    }

    @Override
    public List<Equipment> selectByPrimaryKey(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.EquipmentMapper.selectByPrimaryKey", id);
    }

    @Override
    public List<Equipment> selectAllEquipmentInfo(Equipment equipment) {
        return baseDao.selectList("com.qtummatrix.dao.EquipmentMapper.selectAllEquipmentInfo", equipment);
    }

    @Override
    public int updateByPrimaryKeySelective(Equipment record) {
        return baseDao.update("com.qtummatrix.dao.EquipmentMapper.updateByPrimaryKeySelective", record);
    }

    @Override
    public Collection selectEquipmentInfoExcel(String[] ids) {
        Collection collection = new ArrayList();
        List<EquipmentVo> list = null;
        for (String s : ids) {
            // 查询出所选择的id对应的商品，放入集合中
            list = baseDao.selectList("com.qtummatrix.dao.EquipmentMapper.selectByPrimaryKey", s);
            EquipmentVo equipmentVo = list.get(0);
            collection.add(equipmentVo);
        }
        return collection;
    }

    @Override
    public int uploadImage(Integer id, String imagePath) {
        map.put("id", id);
        map.put("photo", imagePath);
        return baseDao.update("com.qtummatrix.dao.EquipmentMapper.updateByPrimaryKeySelective", map);
    }

    @Override
    public int cancelBx(Integer id) {
        return baseDao.update("com.qtummatrix.dao.EquipmentMapper.cancelBx", id);
    }

    @Override
    public int cancelBxInfo(Integer id) {
        map.put("id", id);
        map.put("time", new Date());
        return baseDao.update("com.qtummatrix.dao.EquipmentMapper.cancelBxInfo", map);
    }

    @Override
    public int updateIsBx(Integer id) {
        return baseDao.update("com.qtummatrix.dao.EquipmentMapper.updateIsBx", id);
    }

    @Override
    public int insertBxInfo(RepairLog repairLog) {
        return baseDao.insert("com.qtummatrix.dao.EquipmentMapper.insertBxInfo", repairLog);
    }

    @Override
    public List<RepairLog> selectRepairLog() {
        return baseDao.selectList("com.qtummatrix.dao.EquipmentMapper.selectRepairLog");
    }

    @Override
    public int batchDeleteData(String[] ids) {
        for (String id : ids) {
            baseDao.update("com.qtummatrix.dao.EquipmentMapper.deleteByPrimaryKey", id);
        }
        return 1;
    }
}