package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.NoticeMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.Notice;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-21 11:21
 */
@Repository
public class NoticeMapperImpl implements NoticeMapper {

    @Autowired
    private BaseDao baseDao;


    @Override
    public int deleteByPrimaryKey(Integer id) {
        return baseDao.update("com.qtummatrix.dao.NoticeMapper.deleteByPrimaryKey", id);
    }

    @Override
    public int insertSelective(Notice record) {
        return baseDao.insert("com.qtummatrix.dao.NoticeMapper.insertSelective", record);
    }

    @Override
    public List<Notice> getAllNotice(@Param(value = "name") String name) {
        return baseDao.selectList("com.qtummatrix.dao.NoticeMapper.getAllNotice", name);
    }

    @Override
    public int updateByPrimaryKeySelective(Notice record) {
        return baseDao.update("com.qtummatrix.dao.NoticeMapper.updateByPrimaryKeySelective", record);
    }

    @Override
    public List<Notice> getAllNoticeById(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.NoticeMapper.getAllNoticeById", id);
    }

    @Override
    public List<Notice> getAllTrashNotice(String name) {
        return baseDao.selectList("com.qtummatrix.dao.NoticeMapper.getAllTrashNotice", name);
    }

    @Override
    public int cancelDeleteNoticeById(Integer id) {
        return baseDao.update("com.qtummatrix.dao.NoticeMapper.cancelDeleteNoticeById", id);
    }
}
