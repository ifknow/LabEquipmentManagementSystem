package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.UserMapper;
import com.qtummatrix.entity.User;
import com.qtummatrix.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-23 18:44
 */
@Service
public class UserLoginServiceImpl implements UserLoginService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> validateLogin(String userAccount, String userPassword) {
        return userMapper.selectUserList(userAccount, userPassword);
    }
}
