package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.EquipmentBorrowMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.EquipmentBorrow;
import com.qtummatrix.entity.Equipmentlog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-23 14:02
 */
@Repository
public class EquipmentBorrowMapperImpl implements EquipmentBorrowMapper {

    @Autowired
    private BaseDao baseDao;

    private Map<String, Object> map = new HashMap<>();

    @Override
    public List<EquipmentBorrow> selectAllEquipmentInfo(String xh, String zzs) {
        map.put("xh", xh);
        map.put("zzs", zzs);
        return baseDao.selectList("com.qtummatrix.dao.EquipmentBorrowMapper.selectAllEquipmentInfo", map);
    }

    @Override
    public List<Equipment> selectEquipmentInfoById(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.EquipmentBorrowMapper.selectEquipmentInfoById", id);
    }

    @Override
    public int backEquipment(Map<String, Object> map) {
        return baseDao.update("com.qtummatrix.dao.EquipmentBorrowMapper.backEquipment", map);
    }

    @Override
    public int borrowEquipment(Map<String, Object> map) {
        return baseDao.update("com.qtummatrix.dao.EquipmentBorrowMapper.borrowEquipment", map);
    }

    @Override
    public int addEquipmentBorrowLog(Integer id, Integer userId, Date date) {
        map.put("userId", userId);
        map.put("equipmentId", id);
        map.put("time", date);
        return baseDao.insert("com.qtummatrix.dao.EquipmentBorrowMapper.addEquipmentBorrowLog", map);
    }

    @Override
    public List<Equipmentlog> selectLogEquipment(Integer userId, Integer equipmentId) {
        map.put("userId", userId);
        map.put("equipmentId", equipmentId);
        return baseDao.selectList("com.qtummatrix.dao.EquipmentBorrowMapper.selectLogEquipment", map);
    }

    @Override
    public int addEquipmentReturnLog(Integer id,  Date date) {
        map.put("id", id);
        map.put("time", date);
        return baseDao.insert("com.qtummatrix.dao.EquipmentBorrowMapper.addEquipmentReturnLog", map);
    }
}
