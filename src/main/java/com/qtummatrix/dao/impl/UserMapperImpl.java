package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.UserMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.User;
import com.qtummatrix.entity.UserXO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-21 16:38
 */
@Repository
public class UserMapperImpl implements UserMapper {

    @Autowired
    private BaseDao baseDao;

    private Map<String, Object> map = new HashMap<>();

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return baseDao.delete("com.qtummatrix.dao.UserMapper.deleteByPrimaryKey", id);
    }

    @Override
    public int insert(User record) {
        return 0;
    }

    @Override
    public int insertSelective(UserXO record) {
        return baseDao.insert("com.qtummatrix.dao.UserMapper.insertSelective", record);
    }

    @Override
    public List<UserXO> selectAllUserList(Map map) {
        return baseDao.selectList("com.qtummatrix.dao.UserMapper.selectAllUserList", map);
    }

    @Override
    public List<UserXO> selectAllAdminList(Map map) {
        return baseDao.selectList("com.qtummatrix.dao.UserMapper.selectAllAdminList", map);
    }

    @Override
    public List<UserXO> selectAllUserListByPrimaryKey(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.UserMapper.selectAllUserListByPrimaryKey", id);
    }

    @Override
    public int updateByPrimaryKeySelective(UserXO record) {
        return baseDao.update("com.qtummatrix.dao.UserMapper.updateByPrimaryKeySelective", record);
    }

    @Override
    public int updateByPrimaryKey(User record) {
        return baseDao.update("com.qtummatrix.dao.UserMapper.updateByPrimaryKey", record);
    }

    @Override
    public List<UserXO> validatePhone(String phone) {
        return baseDao.selectList("com.qtummatrix.dao.UserMapper.validatePhone", phone);
    }

    @Override
    public List<User> selectUserPassword(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.UserMapper.selectUserPassword", id);
    }

    @Override
    public List<User> selectUserList(String userAccount, String userPassword) {
        map.put("userAccount", userAccount);
        map.put("userPassword", userPassword);
        return baseDao.selectList("com.qtummatrix.dao.UserMapper.selectUserList", map);
    }

    @Override
    public int registerUser(User user) {
        return baseDao.insert("com.qtummatrix.dao.UserMapper.registerUser", user);
    }

    @Override
    public int checkUser(Integer id) {
        return baseDao.update("com.qtummatrix.dao.UserMapper.checkUser", id);
    }

    @Override
    public int resetPassWord(Integer id) {
        return baseDao.update("com.qtummatrix.dao.UserMapper.resetPassWord", id);
    }
}
