package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.RepairLogMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.RepairLog;
import com.qtummatrix.entity.RepairLogXO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * create by Gongshiyong  2020-01-23 12:04
 */
@Repository
public class RepairLogMapperImpl implements RepairLogMapper {

    @Autowired
    private BaseDao baseDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(RepairLog record) {
        return 0;
    }

    @Override
    public int insertSelective(RepairLog record) {
        return 0;
    }

    @Override
    public RepairLog selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(RepairLog record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(RepairLog record) {
        return 0;
    }

    @Override
    public List<RepairLogXO> selectAllRepairLog(RepairLogXO repairLogxo) {
        return baseDao.selectList("com.qtummatrix.dao.RepairLogMapper.selectAllRepairLog", repairLogxo);
    }
}
