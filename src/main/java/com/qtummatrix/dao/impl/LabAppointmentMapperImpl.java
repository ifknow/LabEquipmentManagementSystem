package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.LabAppointmentMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.Laboratory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-22 13:20
 */
@Repository
public class LabAppointmentMapperImpl implements LabAppointmentMapper {

    @Autowired
    private BaseDao baseDao;

    @Override
    public List<Laboratory> selectAllLabInfo(Map map) {
        return baseDao.selectList("com.qtummatrix.dao.LabAppointmentMapper.selectAllLabInfo", map);
    }

    @Override
    public int appointment(Map map) {
        return baseDao.update("com.qtummatrix.dao.LabAppointmentMapper.appointment", map);
    }

    @Override
    public int cancelappointment(Map map) {
        return baseDao.update("com.qtummatrix.dao.LabAppointmentMapper.cancelappointment", map);
    }

    @Override
    public List<Laboratory> selectLabInfoById(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.LabAppointmentMapper.selectLabInfoById", id);
    }

    @Override
    public int insertLabAppointInfo(Map<String, Object> map) {
        return baseDao.insert("com.qtummatrix.dao.LabAppointmentMapper.insertLabAppointInfo", map);
    }

    @Override
    public int insertLabAppointInfo1(Map<String, Object> map) {
        return baseDao.insert("com.qtummatrix.dao.LabAppointmentMapper.insertLabAppointInfo1", map);
    }
}
