package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.LaboratoryMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.Laboratory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-22 10:57
 */
@Repository
public class LaboratoryMapperImpl implements LaboratoryMapper {

    @Autowired
    private BaseDao baseDao;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return baseDao.update("com.qtummatrix.dao.LaboratoryMapper.deleteByPrimaryKey",id);
    }

    @Override
    public int insertSelective(Laboratory record) {
        return baseDao.insert("com.qtummatrix.dao.LaboratoryMapper.insertSelective",record);
    }

    @Override
    public List<Laboratory> selectAllLabInfo(Map map) {
        return baseDao.selectList("com.qtummatrix.dao.LaboratoryMapper.selectAllLabInfo", map);
    }

    @Override
    public List<Laboratory> selectByPrimaryKey(Integer id) {
        return baseDao.selectList("com.qtummatrix.dao.LaboratoryMapper.selectByPrimaryKey", id);
    }

    @Override
    public int updateByPrimaryKeySelective(Laboratory record) {
        return baseDao.update("com.qtummatrix.dao.LaboratoryMapper.updateByPrimaryKeySelective", record);
    }

    @Override
    public int updateByPrimaryKey(Laboratory record) {
        return 0;
    }

    @Override
    public List<Laboratory> selectAllLabInfoNoPage() {
        return baseDao.selectList("com.qtummatrix.dao.LaboratoryMapper.selectAllLabInfoNoPage");
    }
}
