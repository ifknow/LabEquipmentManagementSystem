package com.qtummatrix.dao.impl;

import com.qtummatrix.dao.EquipmentlogMapper;
import com.qtummatrix.dao.basedao.BaseDao;
import com.qtummatrix.entity.EquipmentLogXO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-23 17:45
 */
@Repository
public class EquipmentLogMapperImpl implements EquipmentlogMapper {

    @Autowired
    private BaseDao baseDao;

    private Map<String, Object> map = new HashMap<>();

    @Override
    public List<EquipmentLogXO> selectAllEquipmentLog(String xh, String JyUserName) {
        map.put("xh", xh);
        map.put("JyUserName", JyUserName);
        return baseDao.selectList("com.qtummatrix.dao.EquipmentlogMapper.selectAllEquipmentLog", map);
    }
}
