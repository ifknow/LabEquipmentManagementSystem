package com.qtummatrix.dao;

import com.qtummatrix.entity.User;
import com.qtummatrix.entity.UserXO;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    //根据id删除用户信息
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    //保存新建的用户信息
    int insertSelective(UserXO record);

    //查询所有的用户信息
    public List<UserXO> selectAllUserList(Map map);

    //根据主键查询所有的普通用户信息
    public List<UserXO> selectAllUserListByPrimaryKey(Integer id);

    //根据主键查询所有的管理员户信息
    public List<UserXO> selectAllAdminList(Map map);

    //修改用户信息
    public int updateByPrimaryKeySelective(UserXO record);

    //修改密码
    public int updateByPrimaryKey(User record);

    //验证手机号
    public List<UserXO> validatePhone(String phone);

    //查询密码
    public List<User> selectUserPassword(Integer id);

    /**
     * 验证登录
     *
     * @param userAccount  用户手机号
     * @param userPassword 用户密码
     * @return
     */
    public List<User> selectUserList(String userAccount, String userPassword);

    /**
     * 注册
     *
     * @param user
     * @return
     */
    public int registerUser(User user);

    /**
     * 管理员审核用户
     *
     * @param id
     * @return
     */
    public int checkUser(Integer id);

    /**
     * 重置密码
     *
     * @param id
     * @return
     */
    public int resetPassWord(Integer id);
}