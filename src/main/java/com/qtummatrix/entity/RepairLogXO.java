package com.qtummatrix.entity;

import java.util.Date;

/**
 * create by Gongshiyong  2020-01-23 12:12
 */
public class RepairLogXO {
    private Integer id;
    private String xh;
    private String title;
    private Date bxTime;
    private String bxTimeStr;
    private Date endTime;
    private String endTimeStr;
    private String wz;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBxTime() {
        return bxTime;
    }

    public void setBxTime(Date bxTime) {
        this.bxTime = bxTime;
    }

    public String getBxTimeStr() {
        return bxTimeStr;
    }

    public void setBxTimeStr(String bxTimeStr) {
        this.bxTimeStr = bxTimeStr;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public String getWz() {
        return wz;
    }

    public void setWz(String wz) {
        this.wz = wz;
    }
}
