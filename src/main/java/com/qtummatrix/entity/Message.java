package com.qtummatrix.entity;

import java.util.Date;

public class Message {
    private Integer id;

    private String count;

    private Integer isdelete;

    private String msg;

    private Date time;

    private Integer userFsr;

    private Integer userJsr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count == null ? null : count.trim();
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg == null ? null : msg.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getUserFsr() {
        return userFsr;
    }

    public void setUserFsr(Integer userFsr) {
        this.userFsr = userFsr;
    }

    public Integer getUserJsr() {
        return userJsr;
    }

    public void setUserJsr(Integer userJsr) {
        this.userJsr = userJsr;
    }
}