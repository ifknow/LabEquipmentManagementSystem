package com.qtummatrix.entity;

public class EquipmentlogWithBLOBs extends Equipmentlog {
    private byte[] qx;

    private byte[] user;

    public byte[] getQx() {
        return qx;
    }

    public void setQx(byte[] qx) {
        this.qx = qx;
    }

    public byte[] getUser() {
        return user;
    }

    public void setUser(byte[] user) {
        this.user = user;
    }
}