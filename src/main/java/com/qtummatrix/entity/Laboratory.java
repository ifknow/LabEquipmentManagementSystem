package com.qtummatrix.entity;

import java.util.Date;

public class Laboratory {
    private Integer id;

    private String address;

    private String bz;

    private String fzr;

    private String fzrDh;

    private Integer isDelete;

    private Integer isYy;

    private Integer laboratoryLogId;

    private String name;

    private Date time;

    private String timeStr;

    private Integer userId;

    private String realname;

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Integer getId() {
        return id;
    }

    public String getFzrDh() {
        return fzrDh;
    }

    public void setFzrDh(String fzrDh) {
        this.fzrDh = fzrDh;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getIsYy() {
        return isYy;
    }

    public void setIsYy(Integer isYy) {
        this.isYy = isYy;
    }

    public Integer getLaboratoryLogId() {
        return laboratoryLogId;
    }

    public void setLaboratoryLogId(Integer laboratoryLogId) {
        this.laboratoryLogId = laboratoryLogId;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz == null ? null : bz.trim();
    }

    public String getFzr() {
        return fzr;
    }

    public void setFzr(String fzr) {
        this.fzr = fzr == null ? null : fzr.trim();
    }

    public String getFzrdh() {
        return fzrDh;
    }

    public void setFzrdh(String fzrdh) {
        this.fzrDh = fzrdh == null ? null : fzrdh.trim();
    }

    public Integer getIsdelete() {
        return isDelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isDelete = isdelete;
    }

    public Integer getIsyy() {
        return isYy;
    }

    public void setIsyy(Integer isyy) {
        this.isYy = isyy;
    }

    public Integer getLaboratorylogid() {
        return laboratoryLogId;
    }

    public void setLaboratorylogid(Integer laboratorylogid) {
        this.laboratoryLogId = laboratorylogid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}