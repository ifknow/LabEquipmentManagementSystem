package com.qtummatrix.entity;

import lombok.Data;

import java.util.Date;

@Data
public class EquipmentVo {
    private Integer id;

    private String bz;

    private Integer isBx;

    private Integer isDelete;

    private Double jg;

    private String sbxlh;

    private Date time;

    private String xh;

    private String zzs;

    private Integer laboratoryId;

    private Date fwTime;

    private Integer isFw;

    private String photo;

    private Integer bxLogId;

    private Integer isJy;

    private Integer jyId;

    private Integer jyUserId;

}