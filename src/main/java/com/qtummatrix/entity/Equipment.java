package com.qtummatrix.entity;

import java.util.Date;

public class Equipment {
    private Integer id;

    private String bz;

    private Integer isBx;

    private Integer isDelete;

    private Double jg;

    private String sbxlh;

    private Date time;

    private String xh;

    private String zzs;

    private Integer laboratoryId;

    private Date fwTime;

    private String ht;

    private Integer isFw;

    private String photo;

    private Integer bxLogId;

    private Integer isJy;

    private Integer jyId;

    private Integer jyUserId;

    private String timeStr;

    private String timeStrFw;

    public String getTimeStrFw() {
        return timeStrFw;
    }

    public void setTimeStrFw(String timeStrFw) {
        this.timeStrFw = timeStrFw;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public Integer getIsBx() {
        return isBx;
    }

    public void setIsBx(Integer isBx) {
        this.isBx = isBx;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Double getJg() {
        return jg;
    }

    public void setJg(Double jg) {
        this.jg = jg;
    }

    public String getSbxlh() {
        return sbxlh;
    }

    public void setSbxlh(String sbxlh) {
        this.sbxlh = sbxlh;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getZzs() {
        return zzs;
    }

    public void setZzs(String zzs) {
        this.zzs = zzs;
    }

    public Integer getLaboratoryId() {
        return laboratoryId;
    }

    public void setLaboratoryId(Integer laboratoryId) {
        this.laboratoryId = laboratoryId;
    }

    public Date getFwTime() {
        return fwTime;
    }

    public void setFwTime(Date fwTime) {
        this.fwTime = fwTime;
    }

    public String getHt() {
        return ht;
    }

    public void setHt(String ht) {
        this.ht = ht;
    }

    public Integer getIsFw() {
        return isFw;
    }

    public void setIsFw(Integer isFw) {
        this.isFw = isFw;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getBxLogId() {
        return bxLogId;
    }

    public void setBxLogId(Integer bxLogId) {
        this.bxLogId = bxLogId;
    }

    public Integer getIsJy() {
        return isJy;
    }

    public void setIsJy(Integer isJy) {
        this.isJy = isJy;
    }

    public Integer getJyId() {
        return jyId;
    }

    public void setJyId(Integer jyId) {
        this.jyId = jyId;
    }

    public Integer getJyUserId() {
        return jyUserId;
    }

    public void setJyUserId(Integer jyUserId) {
        this.jyUserId = jyUserId;
    }
}