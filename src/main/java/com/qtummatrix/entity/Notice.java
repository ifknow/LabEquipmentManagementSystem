package com.qtummatrix.entity;

import java.util.Date;

/**
 * 公告
 */
public class Notice {

    //公告id
    private Integer id;
    //是否删除
    private Integer isdelete;
    //公告标题
    private String name;
    //公告内容
    private String content;
    //发布公告的时间
    private Date time;
    private String timeStr;

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String nr) {
        this.content = nr == null ? null : nr.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}