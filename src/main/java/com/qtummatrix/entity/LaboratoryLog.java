package com.qtummatrix.entity;

import java.util.Date;

public class LaboratoryLog {
    private Integer id;

    private Date endtime;

    private String endtimestr;

    private Integer isyy;

    private Date time;

    private String timestr;

    private Integer laboratoryId;

    private Integer userqxId;

    private Integer userId;

    private String name;

    private String fzr;

    private String realname;

    public String getEndtimestr() {
        return endtimestr;
    }

    public void setEndtimestr(String endtimestr) {
        this.endtimestr = endtimestr;
    }

    public String getTimestr() {
        return timestr;
    }

    public void setTimestr(String timestr) {
        this.timestr = timestr;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFzr() {
        return fzr;
    }

    public void setFzr(String fzr) {
        this.fzr = fzr;
    }

    public String getRealName() {
        return realname;
    }

    public void setRealName(String realName) {
        this.realname = realName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public Integer getIsyy() {
        return isyy;
    }

    public void setIsyy(Integer isyy) {
        this.isyy = isyy;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getLaboratoryId() {
        return laboratoryId;
    }

    public void setLaboratoryId(Integer laboratoryId) {
        this.laboratoryId = laboratoryId;
    }

    public Integer getUserqxId() {
        return userqxId;
    }

    public void setUserqxId(Integer userqxId) {
        this.userqxId = userqxId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}