package com.qtummatrix.controller;

import com.qtummatrix.service.LabAppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 实验室预约Controller
 * create by Gongshiyong  2020-01-22 12:24
 */
@Controller
@RequestMapping("labAppointment")
public class LabAppointmentController {

    @Autowired
    private LabAppointmentService labAppointmentService;

    private Map<String, Object> map = new HashMap<>();

    /**
     * 跳转到实验室预约页面
     *
     * @return
     */
    @RequestMapping("toLaboratoryAppointment")
    public ModelAndView toLaboratoryAppointment() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("laboratoryAppointment");
        return modelAndView;
    }

    /**
     * 查询实验室预约信息
     *
     * @param pageNo   页码
     * @param pageSize 一页显示多少条数据
     * @param name     实验室名称
     * @param fzr      负责人
     * @return
     */
    @ResponseBody
    @GetMapping("getAllLabList")
    public Object getAllLabList(Integer pageNo, Integer pageSize, String name, String fzr, Integer isYy) {
        return labAppointmentService.selectAllLabInfo(pageNo, pageSize, name, fzr, isYy);
    }

    /**
     * 预约实验室
     *
     * @param id     实验室id
     * @param userId 预约人id
     * @return
     */
    @ResponseBody
    @PutMapping("appointment")
    public Object appointment(Integer id, Integer userId) {
        int i = labAppointmentService.appointment(id, userId);
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "操作成功！");
        } else if (i == -1) {
            map.put("resultCode", "201");
            map.put("resultMessage", "请预约人操作！");
        }
        return map;
    }
}
