package com.qtummatrix.controller;

import com.qtummatrix.entity.Laboratory;
import com.qtummatrix.service.LaboratoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 实验室列表Controller
 * create by Gongshiyong  2020-01-22 10:22
 */
@Controller
@RequestMapping("labList")
public class LaboratoryController {

    @Autowired
    private LaboratoryService laboratoryService;

    private Map<String, Object> map = new HashMap<>();

    /**
     * 从main页面跳转到laboratoryList页面显示实验室列表
     *
     * @return
     */
    @RequestMapping("toLaboratoryList")
    public ModelAndView toLaboratoryList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("laboratoryList");
        return modelAndView;
    }

    /**
     * 查询实验室
     *
     * @param pageNo   页码
     * @param pageSize 一页显示多少条数据
     * @param name     实验室名称
     * @param fzr      负责人
     * @return
     */
    @ResponseBody
    @GetMapping("getAllLabList")
    public Object getAllLabList(Integer pageNo, Integer pageSize, String name, String fzr) {
        return laboratoryService.selectAllLabInfo(pageNo, pageSize, name, fzr);
    }

    /**
     * 根据主键查询实验室信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("selectByPrimaryKey")
    public Object selectByPrimaryKey(Integer id) {
        return laboratoryService.selectByPrimaryKey(id);
    }

    /**
     * 保存编辑后的实验室信息和新建实验室信息
     *
     * @param laboratory
     * @return
     */
    @ResponseBody
    @PutMapping("updateByPrimaryKeySelective")
    public Object updateByPrimaryKeySelective(Laboratory laboratory) {
        int i = 0;
        if (laboratory.getId() != null) {
            //执行编辑操作
            i = laboratoryService.updateByPrimaryKeySelective(laboratory);
        } else {
            //执行添加操作
            i = laboratoryService.insertSelective(laboratory);
        }
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "保存成功！");
        } else {
            map.put("resultCode", "201");
            map.put("resultMessage", "保存失败！");
        }
        return map;
    }

    /**
     * 根据主键删除实验室信息
     *
     * @param id    实验室id
     * @return
     */
    @ResponseBody
    @DeleteMapping("deleteByPrimaryKey")
    public Object deleteByPrimaryKey(Integer id) {
        int i = laboratoryService.deleteByPrimaryKey(id);
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "删除成功！");
        } else {
            map.put("resultCode", "201");
            map.put("resultMessage", "删除失败！");
        }
        return map;
    }
}
