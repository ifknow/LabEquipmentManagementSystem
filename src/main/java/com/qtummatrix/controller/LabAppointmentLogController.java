package com.qtummatrix.controller;

import com.qtummatrix.service.LabAppointmentLogService;
import com.qtummatrix.service.LabAppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 实验室预约记录
 */
@Controller
@RequestMapping("LaboratoryAppointmentLog")
public class LabAppointmentLogController {

    @Autowired
    private LabAppointmentLogService labAppointmentLogService;

    /**
     * 跳转到实验室预约页面
     *
     * @return
     */
    @RequestMapping("skiptoLaboratoryAppointmentLogPage")
    public ModelAndView toLaboratoryAppointmentLog() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("laboratoryAppointmentLog");
        return modelAndView;
    }

    /**
     * 查询所有的实验室预约日志信息
     *
     * @param pageNo   当前页码
     * @param pageSize 一页显示多少条记录
     * @param name     查询条件：实验室名称
     * @param fzr      查询条件：实验室负责人
     * @return
     */
    @ResponseBody
    @GetMapping("getAllLabLogList")
    public Object getAllLabLogList(Integer pageNo, Integer pageSize, String name, String fzr) {
        return labAppointmentLogService.selectAllLabLogInfo(pageNo, pageSize, name, fzr);
    }
}
