package com.qtummatrix.controller;

import com.qtummatrix.entity.UserXO;
import com.qtummatrix.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-21 15:44
 */
@Controller
@RequestMapping("userList")
public class UserListController {

    @Autowired
    private UserService userService;

    private Map<String, Object> map = new HashMap<>();

    /**
     * 跳转到普通用户列表页面
     *
     * @return
     */
    @GetMapping("toUserList")
    public ModelAndView toUserList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("userList");
        return modelAndView;
    }

    /**
     * 跳转到管理员列表页面
     *
     * @return
     */
    @GetMapping("toAdminList")
    public ModelAndView toAdminList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminList");
        return modelAndView;
    }

    /**
     * 显示所有的普通用户信息
     *
     * @param pageNo
     * @param pageSize
     * @param stunumber
     * @param phone
     * @param classs
     * @param realname
     * @return
     */
    @ResponseBody
    @GetMapping("selectAllUserList")
    public Object selectAllUserList(Integer pageNo, Integer pageSize, String stunumber, String phone, String classs,
                                    String realname, Integer userId) {
        return userService.selectAllUserList(pageNo, pageSize, stunumber, phone, classs, realname, userId);
    }

    /**
     * 显示所有的管理员信息
     *
     * @param pageNo
     * @param pageSize
     * @param stunumber
     * @param phone
     * @param classs
     * @param realname
     * @return
     */
    @ResponseBody
    @GetMapping("selectAllAdminList")
    public Object selectAllAdminList(Integer pageNo, Integer pageSize, String stunumber, String phone, String classs,
                                     String realname, Integer userId) {
        return userService.selectAllAdminList(pageNo, pageSize, stunumber, phone, classs, realname, userId);
    }

    /**
     * 根据主键查询所有的用户信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("selectAllUserListByPrimaryKey")
    public Object selectAllUserListByPrimaryKey(Integer id) {
        return userService.selectAllUserListByPrimaryKey(id);
    }

    /**
     * 保存编辑后信息
     *
     * @param userXO
     * @return
     */
    @ResponseBody
    @PutMapping("updateByPrimaryKeySelective")
    public Object updateByPrimaryKeySelective(UserXO userXO) {
        int i = userService.updateByPrimaryKeySelective(userXO);
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "信息修改成功");
        } else {
            map.put("resuleCode", "201");
            map.put("resultMessage", "信息修改失败");
        }
        return map;
    }

    /**
     * 跳转添加用户页面
     *
     * @return
     */
    @GetMapping("skipToUserAdd")
    public ModelAndView skipToUserAdd() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("userAdd");
        return modelAndView;
    }

    /**
     * 跳转添加管理员页面
     *
     * @return
     */
    @GetMapping("skipToAdminAdd")
    public ModelAndView skipToAdminAdd() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminAdd");
        return modelAndView;
    }

    /**
     * 返回到用户信息页面
     *
     * @return
     */
    @GetMapping("skipToUserList")
    public ModelAndView skipToUserList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("userList");
        return modelAndView;
    }

    /**
     * 返回到管理员信息页面
     *
     * @return
     */
    @GetMapping("skipToAdminList")
    public ModelAndView skipToAdminList() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminList");
        return modelAndView;
    }

    /**
     * 添加用户信息
     *
     * @param userXO
     * @return
     */
    @ResponseBody
    @PostMapping("addUser")
    public Object addUser(UserXO userXO) {
        if (userXO.getRolename().equals("管理员")) {
            userXO.setRolename("1");
            userXO.setIssh(1);
        }
        int i = userService.insertSelective(userXO);
        if (i > 0) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }

    /**
     * 验证手机号是否存在
     *
     * @param phone
     * @return
     */
    @ResponseBody
    @GetMapping("validatePhone")
    public Object validatePhone(String phone) {
        int i = userService.validatePhone(phone);
        if (i == 1) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }

    /**
     * 根据id删除用户信息
     *
     * @param id
     * @return
     */
    @ResponseBody
    @DeleteMapping("deleteByPrimaryKey")
    public Object deleteByPrimaryKey(Integer id) {
        int i = userService.deleteByPrimaryKey(id);
        if (i > 0) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }

    /**
     * 修改密码
     *
     * @return
     */
    @ResponseBody
    @PutMapping("changePassword")
    public Object changePassword(Integer id, String oldPassword, String password) {
        int i = userService.changePassword(id, oldPassword, password);
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "密码修改成功！");
        } else {
            map.put("resultCode", "201");
            map.put("resultMessage", "原密码错误，修改失败！");
        }
        return map;
    }

    /**
     * 管理员审核用户
     *
     * @param id
     * @return
     */
    @ResponseBody
    @PutMapping("checkUser")
    public Map checkUser(Integer id) {
        int i = userService.checkUser(id);
        if (i > 0) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }

    /**
     * 管理员审核用户
     *
     * @param id
     * @return
     */
    @ResponseBody
    @PutMapping("resetPassWord")
    public Map resetPassWord(Integer id) {
        int i = userService.resetPassWord(id);
        if (i > 0) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }


}
