package com.qtummatrix.controller;

import com.qtummatrix.entity.Notice;
import com.qtummatrix.entity.User;
import com.qtummatrix.service.InformManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * 通知管理Controller<br>
 * create by Gongshiyong  2020-01-21 9:32
 */
@Controller
@RequestMapping("InformManagement")
public class InformManagementController {

    @Autowired
    private InformManagementService informManagementService;

    private Map<String, Object> map = new HashMap<String, Object>();

    /**
     * 跳转到通知管理页面
     *
     * @return 返回值为InformManagement，通过视图解析器解析，跳转到InformManagement页面
     */
    @RequestMapping("toInformManagement")
    public ModelAndView toInformManagement() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("InformManagement");
        return modelAndView;
    }

    /**
     * 查询所有的通知信息
     *
     * @param pageNo   当前页码
     * @param pageSize 一页显示多少条记录
     * @param name     条件查询的条件：标题
     * @return 返回值为PageInfo对象，对象中存储着查询出来的通知信息和相应的分页数据
     */
    @ResponseBody
    @GetMapping("getAllNotice")
    public Object getAllNotice(Integer pageNo, Integer pageSize, String name, HttpServletRequest request) {
        return informManagementService.getAllNotice(pageNo, pageSize, name);
    }

    /**
     * 根据id查询通知信息
     *
     * @param id 通知id
     * @return 返回值为通过通知id查询到的通知信息，为List< Notice>
     */
    @ResponseBody
    @GetMapping("getAllNoticeById")
    public Object getAllNotice(Integer id) {
        return informManagementService.getAllNoticeById(id);
    }

    /**
     * 保存编辑后的信息或者保存新建信息
     *
     * @param notice notice对象
     * @return
     */
    @ResponseBody
    @PutMapping("saveEidtNotice")
    public Object saveEidtNotice(Notice notice) {
        if (notice.getId() != null) {
            int i = informManagementService.updateByPrimaryKeySelective(notice);
            if (i > 0) {
                map.put("resultCode", "200");
                map.put("resultMessage", "编辑成功！");
            } else {
                map.put("resultCode", "201");
                map.put("resultMessage", "编辑失败！");
            }
        } else {
            int i = informManagementService.insertSelective(notice);
            if (i > 0) {
                map.put("resultCode", "200");
                map.put("resultMessage", "添加成功！");
            } else {
                map.put("resultCode", "201");
                map.put("resultMessage", "添加失败！");
            }
        }
        return map;
    }

    /**
     * 根据id软删除公告信息
     *
     * @param id 通知id
     * @return
     */
    @ResponseBody
    @DeleteMapping("deleteByPrimaryKey")
    public Object deleteByPrimaryKey(Integer id) {
        int i = informManagementService.deleteByPrimaryKey(id);
        if (i > 0) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }
}
