package com.qtummatrix.controller;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.service.EquipmentBorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 设备借出返还信息Controller<br>
 * create by Gongshiyong  2020-01-23 13:42
 */
@Controller
@RequestMapping("equipmentBorrowManagement")
public class EquipmentBorrowManagementController {

    @Autowired
    private EquipmentBorrowService equipmentService;

    private Map<String, Object> map = new HashMap<>();

    /**
     * 从主页跳转到设备借用管理页面
     *
     * @return
     */
    @GetMapping("toEquipmentBorrowManagement")
    public ModelAndView toEquipmentBorrowManagement() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("equipmentBorrowManagement");
        return modelAndView;
    }

    /**
     * 查询所有的设备借用信息
     *
     * @param pageNo   当前页码
     * @param pageSize 一页显示多少条数据
     * @param xh       设备型号
     * @param zzs      设备制造商
     * @return
     */
    @ResponseBody
    @GetMapping("getAllEquipmentBorrowInfo")
    public PageInfo getAllEquipmentBorrowInfo(Integer pageNo, Integer pageSize, String xh, String zzs) {
        return equipmentService.selectAllEquipmentInfo(pageNo, pageSize, xh, zzs);
    }

    /**
     * 归还或借用设备
     *
     * @param id     设备id
     * @param userId 用户id
     * @return
     */
    @ResponseBody
    @PutMapping("backEquipment")
    public Map<String, Object> backEquipment(Integer id, Integer userId) {
        int i = equipmentService.equipmentBackAndBorrow(id, userId);
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "操作成功！");
        } else if (i == -1) {
            map.put("resultCode", "201");
            map.put("resultMessage", "请借用人归还！");
        }
        return map;
    }
}
