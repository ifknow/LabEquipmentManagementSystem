package com.qtummatrix.controller;

import com.qtummatrix.entity.User;
import com.qtummatrix.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-20 9:54
 */
@Controller
@RequestMapping("login")
public class UserLoginController {

    private Map<String, Object> map = new HashMap<>();

    @Autowired
    private UserLoginService userLoginService;

    @RequestMapping("login")
    public ModelAndView toLoginPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /**
     * 验证账号和密码是否正确
     *
     * @param userAccount
     * @param userPassword
     * @return
     */
    @ResponseBody
    @RequestMapping("verify")
    public Map<String, Object> verify(String userAccount, String userPassword, HttpServletRequest request) {
        List<User> userList = userLoginService.validateLogin(userAccount, userPassword);
        if (userList.size() == 0) {
            map.put("resultCode", "201");
            map.put("resultMessage", "手机号或密码错误！");
        } else {
            if (userList.get(0).getIscheck() == 0) {
                map.put("resultCode", "202");
                map.put("resultMessage", "账号未通过审核，请联系管理员审核！");
            } else {
                map.put("resultCode", "200");
                map.put("data", userList.get(0));
                map.put("resultMessage", "登录成功！");

                HttpSession session = request.getSession();
                session.setAttribute("LOGIN_USER", userList.get(0));
            }
        }
        return map;
    }

    /**
     * 登录成功跳转到主页
     *
     * @return
     */
    @RequestMapping("main")
    public ModelAndView toMain() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("main");
        return modelAndView;
    }


}
