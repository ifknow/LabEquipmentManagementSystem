package com.qtummatrix.controller;

import com.qtummatrix.entity.Notice;
import com.qtummatrix.service.InformManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * create by Gongshiyong  2020-01-21 9:32
 */
@Controller
@RequestMapping("Welcome")
public class WelcomeController {


    @RequestMapping("toWelcomePage")
    public ModelAndView toInformManagement() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("welcome");
        return modelAndView;
    }

}
