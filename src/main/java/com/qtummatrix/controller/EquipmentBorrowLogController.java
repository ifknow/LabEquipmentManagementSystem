package com.qtummatrix.controller;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.service.EquipmentLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 设备借出返还日志信息Controller<br>
 * create by Gongshiyong  2020-01-23 17:14
 */
@Controller
@RequestMapping("equipmentBorrowLog")
public class EquipmentBorrowLogController {

    @Autowired
    private EquipmentLogService equipmentLogService;

    /**
     * 从主页跳转到设备借用日志页面
     *
     * @return
     */
    @GetMapping("toEquipmentBorrowLog")
    public ModelAndView toEquipmentBorrowLog() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("equipmentBorrowLog");
        return modelAndView;
    }

    /**
     * 查询所有的设备借用详情
     *
     * @param pageNo     当前页码
     * @param pageSize   一页显示多少条数据
     * @param xh         设备型号
     * @param JyUserName 设备借用的用户名
     * @return
     */
    @ResponseBody
    @GetMapping("selectAllEquipmentLog")
    public PageInfo selectAllEquipmentLog(Integer pageNo, Integer pageSize, String xh, String JyUserName) {
        return equipmentLogService.selectAllEquipmentLog(pageNo, pageSize, xh, JyUserName);
    }
}
