package com.qtummatrix.controller;

import com.qtummatrix.entity.User;
import com.qtummatrix.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户注册Controller
 * create by Gongshiyong  2020-01-28 11:16
 */
@Controller
@RequestMapping("register")
public class RegisterController {

    public Map<String, Object> map = new HashMap<>();

    @Autowired
    private UserService userService;

    /**
     * 验证手机号是否重复
     *
     * @param userAccount 手机号
     * @return
     */
    @ResponseBody
    @PostMapping("verify")
    public Map registerUserValidate(String userAccount) {
        int i = userService.validatePhone(userAccount);
        if (i > 0) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
            map.put("resultMessage", "手机号已被注册！");
        }
        return map;
    }

    /**
     * 注册
     *
     * @param user 用户对象
     * @return
     */
    @ResponseBody
    @PostMapping("register")
    public Map register(User user) {
        int i = userService.registerUser(user);
        if (i > 0) {
            map.put("resultCode", "200");
            map.put("resultMessage", "注册成功，等待管理员审核成功之后才能进行登录！");
        } else {
            map.put("resultCode", "201");
            map.put("resultMessage", "手机号已被注册！");
        }
        return map;
    }
}
