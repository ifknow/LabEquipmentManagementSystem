package com.qtummatrix.controller;

import com.qtummatrix.service.InfomManagementTrashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 通知回收站
 */
@Controller
@RequestMapping("InformManagementTrash")
public class InformManagementTrashController {

    @Autowired
    private InfomManagementTrashService infomManagementTrashService;

    private Map<String, Object> map = new HashMap<String, Object>();

    /**
     * 从通知管理页面跳转到通知管理回收站页面
     *
     * @return
     */
    @RequestMapping("skipToInformManagementTrashPage")
    public ModelAndView ToInformManagementTrash() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("InformManagementTrash");
        return modelAndView;
    }

    /**
     * 获取所有的回收站内的公告
     *
     * @param pageNo   当前页码
     * @param pageSize 一页显示几条数据
     * @param name     查询的条件：公告标题
     * @return
     */
    @ResponseBody
    @GetMapping("getAllTrashNotice")
    public Object getAllTrashNotice(Integer pageNo, Integer pageSize, String name) {
        return infomManagementTrashService.getAllTrashNotice(pageNo, pageSize, name);
    }


    @ResponseBody
    @PutMapping("cancelDeleteNoticeById")
    public Object cancelDeleteNoticeById(Integer id) {
        int i = infomManagementTrashService.cancelDeleteNoticeById(id);
        map.put("resultCode", "200");
        return map;
    }


}
