package com.qtummatrix.controller;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.RepairLogXO;
import com.qtummatrix.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 设备报修日志Controller<br>
 * create by Gongshiyong  2020-01-23 11:50
 */
@Controller
@RequestMapping("equipmentRepairLog")
public class EquipmentRepairLogController {

    @Autowired
    private RepairService repairService;

    /**
     * 从主页到设备保修日志页面
     *
     * @return
     */
    @GetMapping("toEquipmentRepairLogInfo")
    public ModelAndView toEquipmentRepairLogInfo() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("equipmentRepairLog");
        return modelAndView;
    }

    /**
     * 查询所有的设备报修日志
     *
     * @param repairLog repairLog报修日志
     * @param pageNo    当前页码
     * @param pageSize  一个显示多少条记录
     * @return
     */
    @ResponseBody
    @GetMapping("selectAllRepairLog")
    public PageInfo selectAllRepairLog(RepairLogXO repairLog, Integer pageNo, Integer pageSize) {
        return repairService.selectAllRepairLog(repairLog, pageNo, pageSize);
    }
}
