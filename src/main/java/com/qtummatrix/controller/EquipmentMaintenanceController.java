package com.qtummatrix.controller;

import com.github.pagehelper.PageInfo;
import com.qtummatrix.entity.Equipment;
import com.qtummatrix.entity.RepairLog;
import com.qtummatrix.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 设备维护Controller<br>
 * create by Gongshiyong  2020-01-23 9:43
 */
@Controller
@RequestMapping("equipmentMaintenance")
public class EquipmentMaintenanceController {

    @Autowired
    private EquipmentService equipmentService;

    private Map<String, Object> map = new HashMap<String, Object>();

    /**
     * 跳转到设备管理页面
     *
     * @return
     */
    @GetMapping("toEquipmentMaintenance")
    public ModelAndView toEquipmentMaintenance() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("equipmentMaintenance");
        return modelAndView;
    }

    /**
     * 查询所有的设备技术参数
     *
     * @param equipment equipment对象
     * @param pageNo    当前页码
     * @param pageSize  一页显示多少条记录
     * @return
     */
    @ResponseBody
    @GetMapping("selectAllEquipmentInfo")
    public PageInfo selectAllEquipmentInfo(Equipment equipment, Integer pageNo, Integer pageSize) {
        return equipmentService.selectAllEquipmentInfo(equipment, pageNo, pageSize);
    }

    /**
     * 取消报修
     *
     * @param id 设备id
     * @return
     */
    @ResponseBody
    @PutMapping("cancelBx")
    public Map cancelBx(Integer id) {
        int i = equipmentService.cancelBx(id);
        if (i == 1) {
            map.put("resultCode", "200");
        } else {
            map.put("resultCode", "201");
        }
        return map;
    }

    /**
     * 添加设备报修信息
     *
     * @param repairLog repairLog对象，存储的是报修具体信息
     * @return
     */
    @ResponseBody
    @PostMapping("addBxInfo")
    public Map<String, Object> addBxInfo(RepairLog repairLog) {
        int i = equipmentService.addBxInfo(repairLog);
        if (i == 1) {
            map.put("resultCode", "200");
            map.put("resultMessage", "报修成功！");
        } else {
            map.put("resultCode", "201");
            map.put("resultCode", "报修失败！");
        }
        return map;
    }


}
