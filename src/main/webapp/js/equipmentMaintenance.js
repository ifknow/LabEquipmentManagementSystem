var grid;
var form;
var form1;
$(function () {

    $(".select2").select2();

    var pageNo = 1;
    var pageSize = 10;
    var grid_selector = "#grid-table";
    var pager_selector = "#grid-pager";
    //初始化数据表格
    grid = $(grid_selector).jqGrid({
        datatype: "json",
        url: '../equipmentMaintenance/selectAllEquipmentInfo',
        mtype: 'GET',
        page: pageNo,
        postData: {
            "xh": $("#equipmentXH").val(),
            "zzs": $("#equipmentZZS").val(),
            "sbxlh": $("#equipmentXLH").val()
        },
        height: 380,
        width: 300,
        colNames: ['设备型号', "设备价格(元)", "设备制造商", "设备序列号", "是否报修", "最后一次报修修时间", "操作"],
        colModel: [
            {name: 'xh', index: 'xh', sortable: false, align: 'center', width: 100},
            {name: 'jg', index: 'jg', sortable: false, align: 'center', width: 80},
            {name: 'zzs', index: 'zzs', sortable: false, align: 'center', width: 100},
            {name: 'sbxlh', index: 'sbxlh', sortable: false, align: 'center', width: 120},
            {
                name: 'isBx', index: 'isBx', fixed: true, sortable: false, resize: false, width: 120,
                formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == 1) {
                        return "<button class='btn btn-default btn-xs')'>报修中</button>";
                    } else {
                        return "<button class='btn btn-primary btn-xs')'>正常使用</button>";
                    }
                }
            },
            {name: 'timeStrFw', index: 'timeStrFw', sortable: false, align: 'center', width: 120},
            {
                name: 'isBx', index: 'isBx', fixed: true, sortable: false, resize: false, width: 100,
                formatter: function (cellvalue, options, rowObject) {
                    if (rowObject.isBx == 1) {
                        return "<button class='btn btn-primary btn-xs' onclick='openEdit(" + rowObject.isBx + "," + rowObject.id + ")'><i class='fa fa-edit button-in-i'></i>恢复使用</button>";
                    } else {
                        return "<button class='btn btn-default btn-xs' onclick='openEdit(" + rowObject.isrecom + "," + rowObject.id + ")'><i class='fa fa-edit button-in-i'></i>点击报修</button>";
                    }
                }
            },
        ],
        viewrecords: true,
        rowNum: pageSize,
        rownumbers: true,//添加左侧行号
        rowList: [10, 15, 20],
        pager: pager_selector,
        altRows: true,
        multiselect: false,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
            }, 0);
            var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);
            /*var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);*/
        },
        autowidth: true,
        jsonReader: jsonReader,   // json中代表实际模型数据的入口
        rownumWidth: 55,
        prmNames: prmNames,
        recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
        emptyrecords: "没有记录",
        loadtext: "正在加载...",
        pgtext: "第 {0} 页  ,共 {1} 页"
    });
    //初始化分页条
    showNavGrid(grid_selector, pager_selector);
    //初始化信息校验表单
    form = $("#addform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    form1 = $("#Editform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });

    //查询
    $("#queryBtn").click(function () {
        var filters = {
            "xh": $("#equipmentXH").val(),
            "zzs": $("#equipmentZZS").val(),
            "sbxlh": $("#equipmentXLH").val()
        }
        grid.jqGrid("setGridParam", {postData: filters, page: 1});
        grid.trigger('reloadGrid');
    });
    //清空
    $("#clearBtn").click(function () {
        $("#equipmentXH").val("");
        $("#equipmentXLH").val("");
        $("#equipmentZZS").val("");
    });

    //保存
    $("#saveBtn").click(function () {
        doSubmit();
    });

});

//打开编辑
function openEdit(isBx, id) {
    //发送ajax请求查询该设备是否报修
    if (isBx == 1) {
        //如果已经报修中，则发送请求取消恢复正常
        bootbox.confirm("您确定要将设备恢复为正常状态？", function (result) {
            if (result) {
                $.ajax({
                    url: "../equipmentMaintenance/cancelBx?id=" + id,
                    type: "PUT",
                    success: function (data) {
                        // console.log(deleteResult);
                        if (data.resultCode == 200) {
                            bootbox.alert("恢复成功！");
                            grid.trigger('reloadGrid');
                        } else {
                            bootbox.alert("恢复失败！");
                            grid.trigger('reloadGrid');
                        }
                    },
                    error: function (data) {
                        bootbox.alert("恢复失败！");
                    }
                })
            }
        });
    } else {
        //否则编辑报修信息
        clearForm();
        $("#editmyModal").modal();
        $("#modeltitle1").html("添加报修记录");
        $("#id").val(id);
    }

}

//保存编辑后的信息
function doSubmit() {
    $("#loading").removeClass('hide').addClass('show');
    $.post("../equipmentMaintenance/addBxInfo?_method=POST", $('#Eidtform').serialize(), function (data) {
        if (data.resultCode == 201) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
            grid.trigger('reloadGrid');
        } else if (data.resultCode == 200) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
            grid.trigger('reloadGrid');
            $('#editmyModal').modal('hide');
        }
    });
}

//清除表单
function clearForm() {
    //清空内容
    $("#id").val("");
    $("#title").val("");
    $("#bz").val("");
    $("#wz").val("");

    //清除验证状态
    $("#addform .form-group").removeClass('has-info').removeClass('has-error');
}

