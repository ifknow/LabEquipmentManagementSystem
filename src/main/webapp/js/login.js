/*
* 登录验证
*/
function login() {
    //验证手机号非空
    if ($("#userAccount").val() == "") {
        bootbox.alert("手机号码不能为空！")
        return;
    }
    //验证手机号格式
    if (isPhoneNo($.trim($("#userAccount").val())) == false) {
        bootbox.alert("手机号码格式不正确！")
        return;
    }
    //验证密码非空

    if ($('#userPassword').val() == "") {
        bootbox.alert("密码不能为空！")
        return;
    }
    //登录验证
    $.ajax({
        url: "login/verify",
        type: "POST",
        // dataType:'json',
        data: {
            "userAccount": $('#userAccount').val(),
            "userPassword": $('#userPassword').val()
        },
        success: function (data) {
            if (data.resultCode == 200) {
                //将当前登录的用户信息保存在localstorage中，方便程序中使用
                localStorage.setItem("userId", data.data.id);
                localStorage.setItem("roleId", data.data.roleid);
                localStorage.setItem("realname", data.data.realname);
                var time = new Date();
                var loginTime = time.toLocaleString(); //获取日期与时间
                localStorage.setItem("loginTime", loginTime);
                window.location.href = "login/main";
            } else {
                bootbox.alert(data.resultMessage)
            }
        //大学生将，，，将会见面


        }
    })
}

// 验证手机号
function isPhoneNo(phone) {
    var pattern = /^1[34578]\d{9}$/;
    return pattern.test(phone);
}