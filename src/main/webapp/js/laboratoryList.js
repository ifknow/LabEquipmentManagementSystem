var grid;
var form;
var form1;
$(function () {

    $(".select2").select2();

    var pageNo = 1;
    var pageSize = 10;
    var grid_selector = "#grid-table";
    var pager_selector = "#grid-pager";
    //初始化数据表格
    grid = $(grid_selector).jqGrid({
        datatype: "json",
        url: '../labList/getAllLabList',
        mtype: 'GET',
        page: pageNo,
        postData: {
            "name": $("#labName").val(),
            "fzr": $("#fzrName").val(),
        },
        height: 380,
        width: 300,
        colNames: ['实验室名称', "负责人", "负责人电话", "地点", "添加时间", "备注", "操作"],
        colModel: [
            {name: 'name', index: 'name', sortable: false, align: 'center', width: 200},
            {name: 'fzr', index: 'fzr', sortable: false, align: 'center', width: 200},
            {name: 'fzrDh', index: 'fzrDh', sortable: false, align: 'center', width: 200},
            {name: 'address', index: 'address', sortable: false, align: 'center', width: 200},
            {name: 'timeStr', index: 'timeStr', sortable: false, align: 'center', width: 200},
            {name: 'bz', index: 'bz', sortable: false, align: 'center', width: 250},
            {
                name: 'id', index: 'id', fixed: true, sortable: false, resize: false, width: 200,
                formatter: function (cellvalue, options, rowObject) {
                    return "<button  class='btn btn-twitter btn-xs' onclick='openEdit(\"" + rowObject.id + "\")'><i class='fa fa-edit button-in-i'></i>编辑</button>"
                        + "<button class='btn btn-danger btn-xs' onclick='openDelete(\"" + rowObject.id + "\")' ><i class='fa fa-trash button-in-i'></i>删除</button>";

                }
            },
        ],
        viewrecords: true,
        rowNum: pageSize,
        rownumbers: true,//添加左侧行号
        rowList: [10, 15, 20],
        pager: pager_selector,
        altRows: true,
        multiselect: false,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
            }, 0);
            var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);
            /*var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);*/
        },
        autowidth: true,
        jsonReader: jsonReader,   // json中代表实际模型数据的入口
        rownumWidth: 55,
        prmNames: prmNames,
        recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
        emptyrecords: "没有记录",
        loadtext: "正在加载...",
        pgtext: "第 {0} 页  ,共 {1} 页"
    });
    //初始化分页条
    showNavGrid(grid_selector, pager_selector);
    //初始化信息校验表单
    form = $("#addform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    form1 = $("#Editform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });

    //添加新实验室
    $("#addNewBtn").click(function () {
        addNewBtn();
    });
    //编辑
    $("#openEdit").click(function () {
        openEdit(str);
    });
    //查询
    $("#queryBtn").click(function () {
        var filters = {
            "name": $("#labName").val(),
            "fzr": $("#fzrName").val(),
        }
        grid.jqGrid("setGridParam", {postData: filters, page: 1});
        grid.trigger('reloadGrid');
    });
    //清空
    $("#clearBtn").click(function () {
        $("#labName").val("");
        $("#fzrName").val("");
    });
    //保存
    $("#saveBtn").click(function () {
        doSubmit();
    });
    $("#fzrDh").blur(function () {
        //验证格式
        if (isPhoneNo($.trim($("#fzrDh").val())) == false) {
            bootbox.alert("手机号码格式不正确！");
        }
    });

});

// 验证手机号
function isPhoneNo(phone) {
    var pattern = /^1[34578]\d{9}$/;
    return pattern.test(phone);
}

function addNewBtn() {
    clearForm();
    $("#editmyModal").modal();
    $("#modeltitle1").html("添加实验室");
    $("#idLable").css("display", "none");


}
//打开编辑表单
function openEdit(id) {
    clearForm();
    $("#editmyModal").modal();
    $("#modeltitle1").html("实验室信息编辑");
    $("#idLable").css("display", "none");
    //Ajax请求查询编辑的信息
    $.post("../labList/selectByPrimaryKey", {
        "id": id,
        "_method": "GET",
    }, function (data) {
        $("#id").val(data[0].id);
        $("#name").val(data[0].name);
        $("#fzr").val(data[0].fzr);
        $("#fzrDh").val(data[0].fzrDh);
        $("#address").val(data[0].address);
        $("#bz").val(data[0].bz);
    })
}

//保存编辑后的信息
function doSubmit() {
    $("#loading").removeClass('hide').addClass('show');
    $.post("../labList/updateByPrimaryKeySelective?_method=PUT", $('#Eidtform').serialize(), function (data) {
        if (data.resultCode == 201) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
        } else if (data.resultCode == 202) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
        } else if (data.resultCode == 200) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
            grid.trigger('reloadGrid');
            $('#editmyModal').modal('hide');
        }
    });
}

//删除记录
function openDelete(str) {
    array = str.split(",");
    bootbox.confirm("您确定要删除该实验室吗?", function (result) {
        if (result) {
            $.post("../labList/deleteByPrimaryKey", {
                "id": str,
                "_method": "DELETE",
            }, function (data) {
                bootbox.alert(data.resultMessage);
                grid.trigger('reloadGrid');
            });
        }
    });
}

//清除表单
function clearForm() {
    //清空内容
    $("#id").val("");
    $("#name").val("");
    $("#fzr").val("");
    $("#fzrDh").val("");
    $("#address").val("");
    $("#bz").val("");
    //清除验证状态
    $("#addform .form-group").removeClass('has-info').removeClass('has-error');
}

