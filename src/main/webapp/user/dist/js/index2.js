var grid;
var arr=[];

$(function(){
	
	$("#loading").removeClass('hide').addClass('show');
	//横向滚动条隐藏
	//$("#grid-table").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });  
	//初始化分页条
//	showNavGrid(grid_selector,pager_selector);
	$.ajax({
		type: "POST",
		//async: false, 
		url: "inorderIndexStatics",
		data: {},
	    success: function(data){
	    	var obj=data;
	    	$("#mechanismCount").html(obj.mechanismCount);
	    	$("#sellerNoAduitCount").html(obj.sellerNoAduitCount);
	    	$("#sellerAduitCount").html(obj.sellerAduitCount);
	    	$("#warehouseCount").html(obj.warehouseCount);
			$("#count").html(obj.count);
			var money="0";
			if(obj.discountAmount)money=obj.discountAmount;
			$("#discountAmount").html(fmoney(money,2)+"元");
			if(obj.firstTime)
				$("#firstTime").html("   "+obj.firstTime);
			else
				$("#firstTime").html("");
			if(obj.lastTime)
				$("#lastTime").html(" 至   "+obj.lastTime);
			else
				$("#lastTime").html("");
			$("#averageMoney").html(fmoney(obj.discountAmount/obj.count,2)+"元");
			$("#loading").removeClass('show').addClass('hide');

		}
	});
	
	$(window).resize(function(){   
        $('#grid-table').setGridWidth($('.grid-table').width());
    });
	
	//var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
	//$('.ui-jqgrid-bdiv').height(h);	
	
	$(".select2").select2({
		allowClear: true
	});
	
//	initWarehouseSelect();
	
	$("#mechanism").click(function(){
		loadPage("mechanism.html?moduleId=1");
	});
	$("#warehouse").click(function(){
		loadPage("warehouse.html?moduleId=5");
	});
	$("#goodsInfo").click(function(){
		loadPage("goodsInfo.html?moduleId=6");
	});
	$("#sellerAudit").click(function(){
		loadPage("sellerAudit.html?moduleId=10");
	});
	$("#realAuthentication").click(function(){
		loadPage("realAuthentication.html?moduleId=14");
	});
	
	
	$("#queryBtn").click(function(){
		var filters = {
				'orderId':$("#orderId").val()
				,'warehouseCode':$("#warehouseCode").val()
				,'startTime':$("#d4311").val()
				,'endTime':$("#d4312").val()
				,'orderState':$("#orderState").val()
				,'orderStep':$("#orderStep").val()
				,'payType':$("#payType").val()}
		         
		grid.jqGrid("setGridParam", { postData: filters,page:1 });
		grid.trigger('reloadGrid');
		
	});
	
	//清空
	$("#clearBtn").click(function(){
		$("#orderId").val("");
		$("#warehouseCode").val(null).trigger("change");
		$("#payType").val(null).trigger("change");
		$("#orderState").val(null).trigger("change");
		$("#orderStep").val(null).trigger("change");
		$("#d4311").val("");
		$("#d4312").val("");
		var filters = {
				'orderId':$("#orderId").val()
				,'warehouseCode':$("#warehouseCode").val()
				,'startTime':$("#d4311").val()
				,'endTime':$("#d4312").val()
				,'orderState':$("#orderState").val()
				,'orderStep':$("#orderStep").val()
				,'payType':$("#payType").val()}
		         
		grid.jqGrid("setGridParam", { postData: filters });
		grid.trigger('reloadGrid');
	});
	
	$("#exportBtn").click(function(){
		 $("#sign").val("d");
         $('#exportfeeform').submit();   
	});
	
	//格式化金额  
    function fmoney(s, n)  
    {  
		if(s==null&&s=="")
			return 0;
		if(!s)
			return 0;
       n = n > 0 && n <= 20 ? n : 2;  
       s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";//更改这里n数也可确定要保留的小数位  
       var l = s.split(".")[0].split("").reverse(),  
       r = s.split(".")[1];  
       t = "";  
       for(i = 0; i < l.length; i++ )  
       {  
          t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");  
       }  
       return t.split("").reverse().join("") + "." + r.substring(0,2);//保留2位小数  如果要改动 把substring 最后一位数改动就可  
    } 

	   
});




function exExcel(){
	// $('#exportfeeform').attr('action','fee/downmotorfee');    
	   // $("#sign").val("b");
	    $("#sign").val("d");
        $('#exportfeeform').submit();   
}

//判断权限功能按钮是否显示
function displayButton(permissions)
{	
	var permissionList=permissions.split(",");
	for(var i=0;i<permissionList.length;i++)
	{
	  if(permissionList[i]=="editBtn" || permissionList[i]=="delBtn" || permissionList[i]=="viewBtn")
	  {
		  $("."+permissionList[i]).show();
	  }	  
	  else
	  {
		  $("#"+permissionList[i]).show();
	  }
	}
}

//初始化权限树
function setPermission(id)
{
	$("#permissionModal").modal();  
	var setting = {
			view: {
				dblClickExpand: false,
				showLine: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
            check: {    
                enable: true  
            }
		};
	
	var zNodes=[];
	$.post("getPermissionTree",{levelId:id
		},function(data){
			zNodes=eval(data.resultMessage);
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		});
	
	$("#levelId").html(id);
}




//初始化仓库下拉框
function initWarehouseSelect()
{
    $.post("getWarehouseList",{
	},function(data){
		for(var i=0;i<data.length;i++){			
			$("#warehouseCode").append('<option value='+data[i].warehouseCode+'>'+data[i].warehouseCode+'</option>');		     
		}
	});
}

//初始化采购单下拉框
function initOrderStepSelect()
{		
	$("#warehouseCode").append('<option value='+data[i].warehouseCode+'>'+data[i].warehouseCode+'</option>');		     
}

//查看采购单详情
function openView(orderId)
{
	var pageNo=grid.getGridParam('page');
	var pageSize=grid.getGridParam("rowNum");
	var pageNum={"pageNo":pageNo,"pageSize":pageSize};
	var pageNumB=JSON.stringify(pageNum);
	localStorage.setItem("pageNum", pageNumB);
	localStorage.setItem("pageFrom", "index2.html?moduleId=8");
	loadPage("inorderInfo.html?moduleId="+$("#moduleId").html()+"&orderId="+orderId);
}


$(function () {	  
    //获得该机构下菜单目录列表
   getCatalogList();
   $("#indexHtml").css("visibility","hidden"); 
	$("#mechanism").css("visibility","hidden"); 
	$("#warehouse").css("visibility","hidden"); 
	$("#goodsInfo").css("visibility","hidden"); 
	$("#sellerAudit").css("visibility","hidden"); 
	$("#realAuthentication").css("visibility","hidden"); 
	$(".search-form").css("visibility","hidden"); 
	$("#form").css("visibility","hidden");
  });

/*
 * 获得该机构下菜单目录列表
 */
function getCatalogList()
{
	
	$.post("getCatalogList",{
		},function(data){
			for(var i=0;i<data.length;i++){
				var cdata= data[i];
				modules = cdata.modules;
				for(var j=0;j<data[i].modules.length;j++){
					var module = data[i].modules[j];
					var moduleName = module.moduleName;
					if("机构管理"==moduleName){
						$("#indexHtml").css("visibility","visible"); 
						$("#mechanism").css("visibility","visible"); 
					}
					if("仓库管理"==moduleName){
						$("#indexHtml").css("visibility","visible");
						$("#warehouse").css("visibility","visible"); 
					}
					if("商品信息管理"==moduleName){
						$("#indexHtml").css("visibility","visible");
						$("#goodsInfo").css("visibility","visible"); 
					}
					if("商家信息管理"==moduleName){
						$("#indexHtml").css("visibility","visible");
						$("#sellerAudit").css("visibility","visible"); 
					}
					if("实名认证"==moduleName){
						$("#indexHtml").css("visibility","visible");
						$("#realAuthentication").css("visibility","visible"); 
					}
					if("采购单管理"==moduleName){
						$("#indexHtml").css("visibility","visible");
						$(".search-form").css("visibility","visible"); 
						$("#form").css("visibility","visible");
					}
//					console.log(module);
				}
			}
			
	
		});
}


