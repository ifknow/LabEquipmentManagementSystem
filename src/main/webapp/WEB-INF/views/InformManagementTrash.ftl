<#--freemark方式引入绝对路径-->
<#assign ctx=springMacroRequestContext.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>通知管理回收站页面</title>
<#--引入js-->
    <script src="${ctx}/js/InformManagementTrash.js" type="text/javascript"/>
</head>
<body>
<!-- content start -->
<section class="content">
    <!-- 分类列表 -->
    <div class="row">
        <div class="col-xs-12">
            <!-- search-form start -->
            <section class="search-form hidden-print" id="search" style="display: block">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-lg-6">
                                <div class="form-group">
                                    <input id="searchTitle" name="searchGoodsId" class="form-control"
                                           style="width: 100%;" placeholder="请输入标题">
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <button id="queryBtn" class="btn btn-primary my-btn"><i
                                        class="fa fa-search button-in-i"></i>查询
                                </button>
                                <button id="clearBtn" class="btn btn-default my-btn"><i
                                        class="fa fa-trash button-in-i"></i>清空
                                </button>
                                <button id="goBack" class="btn btn-default my-btn"><i
                                        class="fa fa-backward button-in-i"></i>返回
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- search-form end -->
            <!-- result-table start -->
            <section class="result-table">
                <div class="box">
                    <div class="grid-table">
                        <!--startprint1-->
                        <table id="grid-table" class="table-striped table-hover"></table>
                        <!--endprint1-->
                        <div id="grid-pager" class="box-footer"></div>
                    </div>
                </div>
            </section>
            <!-- result-table end -->
        </div>
    </div>
</section>
<!-- content end -->

<!--    查看  Modal -->
<div class="modal fade bs-example-modal-lg" id="showmyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="Showform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle">信息查看</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="idLable">
                            <label for="ename" class="col-sm-2 control-label">公告ID</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="noticeId" name="noticeId" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ename" class="col-sm-2 control-label">标题</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="noteTitle" name="noteTitle" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ename" class="col-sm-2 control-label">公告内容</label>
                            <div class="col-sm-7">
                                <textarea type="text" class="form-control" id="noteContent"
                                          name="noteContent" style="width: 500px;height: 200px;" readonly/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-close button-in-i"></i>取消
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>