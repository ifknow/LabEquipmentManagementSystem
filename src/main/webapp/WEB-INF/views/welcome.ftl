<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <style type="text/css">
        body {
            background: #fff;
            font-family: "微软雅黑";
            color: #333;
        }

        h1 {
            font-size: 100px;
            font-weight: normal;
            margin-bottom: 12px;
        }

        p {
            line-height: 1.8em;
            font-size: 36px;
        }
    </style>
</head>
<div style="padding: 24px 48px; margin: auto">
    <p style="padding-left: 25%; padding-top: 200px;">欢迎访问 <b>实验室设备管理系统</b>！</p>
</div>
</html>