<#--freemark方式引入绝对路径-->
<#assign ctx=springMacroRequestContext.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>设备管理</title>
<#--引入js-->
    <script src="${ctx}/js/tecparamManagement.js" type="text/javascript"/>
</head>
<body>
<!-- content start -->
<section class="content">
    <!-- 分类列表 -->
    <div class="row">
        <div class="col-xs-12">
            <!-- search-form start -->
            <section class="search-form hidden-print" id="search" style="display: block">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="equipmentXH" name="equipmentXh" class="form-control"
                                           style="width: 100%;" placeholder="请输入设备型号">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="equipmentZZS" name="equipmentZZS" class="form-control"
                                           style="width: 100%;" placeholder="请输入设备制造商">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="equipmentXLH" name="equipmentXLH" class="form-control"
                                           style="width: 100%;" placeholder="请输入设备序列号">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-2">
                                <select id="labId" name="labId" class="form-control select2" style="width: 100%;"
                                        data-placeholder=" --所属实验室--">
                                </select>
                            </div>
                            <div class="col-sm-2 col-lg-4">
                                <button id="queryBtn" class="btn btn-primary my-btn"><i
                                        class="fa fa-search button-in-i"></i>查询
                                </button>
                                <button id="clearBtn" class="btn btn-default my-btn"><i
                                        class="fa fa-trash button-in-i"></i>清空
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- search-form end -->
            <!-- result-table start -->
            <section class="result-table">
                <div class="box">
                    <div class="box-header table-top-button">
                        <div class="col-xs-2">
                            <button type="submit" class="btn btn-info my-btn" id="addNewEquipment"><i
                                    class="fa fa-plus button-in-i"></i> 新增设备
                            </button>
                        </div>
                        <#--<div class="col-xs-2">
                            <button type="submit" class="btn btn-info my-btn" id="importDataExcel"><i
                                    class="fa fa-plus button-in-i"></i> 批量新增
                            </button>
                        </div>
                        <div class="col-xs-2">
                            <button type="submit" class="btn btn-info my-btn" id="template" onclick="">
                                <i class="fa fa-download button-in-i"></i>
                                模板下载
                            </button>
                        </div>-->
                        <div class="col-xs-2">
                            <button type="submit" class="btn btn-info my-btn" id="exportDataExcel" onclick=""><i
                                    class="fa fa-plus ion-android-exit"></i> 导出数据
                            </button>
                        </div>
                        <div class="col-xs-2">
                            <button type="submit" class="btn btn-info my-btn" id="batchDelete" onclick=""><i
                                    class="fa fa-trash button-in-i"></i> 批量删除
                            </button>
                        </div>
                    </div>
                    <div class="grid-table">
                        <!--startprint1-->
                        <table id="grid-table" class="table-striped table-hover"></table>
                        <!--endprint1-->
                        <div id="grid-pager" class="box-footer"></div>
                    </div>
                </div>
            </section>
            <!-- result-table end -->
        </div>
    </div>
</section>
<!-- content end -->
<!--    图片查看  Modal -->
<div class="modal fade bs-example-modal-lg" id="showMyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="showform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle2">信息查看</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group">
                            <div class="col-sm-7">
                                <img class="form-control" id="photo" src="" style="width: 300px; height: 300px;"/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-close button-in-i"></i>取消
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- content end -->
<!--    上传 Modal -->
<div class="modal fade bs-example-modal-lg" id="uploadModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="uploadForm" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle3">上传设备图片</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group">
                            <label for="goodDetailsImg" class="col-sm-3 control-label">选择设备图片：</label>
                            <div class="col-sm-2">
                                <div id="detailFiles" class="files"></div>
                                <input id="img" type="file" name="ImgPath" class="btn btn-default">
                                <img id="goodsPicture" name="goodsPicture" src=""
                                     style="width: 200px; height: 200px; display: none">
                                <input hidden id="ImagePath" name="ImagePath" value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">操作：</label>
                            <div class="col-sm-2">
                                <input class="btn btn-file" type="button" id="uploadBtn" value="点击上传"/>
                                <input hidden id="equId" value=""/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-close button-in-i"></i>取消
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<#--批量新增-->
<div class="modal fade bs-example-modal-lg" id="importDataExcelModel" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="uploadForm" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle4">批量新增设备</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group">
                            <label for="goodDetailsImg" class="col-sm-3 control-label">选择文件：</label>
                            <div class="col-sm-2">
                                <div id="detailFiles" class="files"></div>
                                <input id="uploadFile" type="file" name="uploadFile" class="btn btn-default">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label">操作：</label>
                            <div class="col-sm-2">
                                <input class="btn btn-file" type="button" id="uploadExcelBtn" value="保存"/>
                                <input hidden id="equId" value=""/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <#--<button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-close button-in-i"></i>取消
                        </button>-->
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!--    编辑  Modal -->
<div class="modal fade bs-example-modal-lg" id="editmyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="Eidtform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle1">实验室信息编辑</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="idLable">
                            <label for="id" class="col-sm-4 control-label">设备ID</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="id" placeholder="设备ID" name="id" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="xh" class="col-sm-4 control-label">设备型号</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="xh" placeholder="请输入设备型号" name="xh"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jg" class="col-sm-4 control-label">设备价格(元)</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="jg" placeholder="请输入设备价格"
                                       name="jg"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="laboratoryId" class="col-sm-4 control-label">所属实验室</label>
                            <div class="col-sm-6 ">
                                <select id="laboratoryId" name="laboratoryId" class="form-control select2"
                                        style="width: 100%;" data-placeholder="--/ 请选择所属实验室/ --">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="zzs" class="col-sm-4 control-label">设备制造商</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="zzs" name="zzs"
                                       placeholder="请输入设备制造商"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sbxlh" class="col-sm-4 control-label">设备序列号</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="sbxlh"
                                       name="sbxlh" placeholder="请输入设备序列号"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bz" class="col-sm-4 control-label">设备用途说明</label>
                            <div class="col-sm-7">
                            <textarea class="form-control" id="bz" name="bz" maxlength="300" rows="5"
                                      placeholder="设备用途说明"/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-close button-in-i"></i>取消
                        </button>
                        <button type="button" class="btn btn-primary" id="saveBtn"><i
                                class="fa fa-check button-in-i"></i>保存
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<#--修改密码-->
<div class="modal fade bs-example-modal-lg" id="indexEditP" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="editPasswordForm" role="form" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="indexModeltitle">修改密码</h4>
                </div>

                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <input type="hidden" id="indexId" name="indexId" value="0"/>
                        <div class="form-group">
                            <label for="indexMechanismCode" class="col-sm-2 control-label">ID</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="userid"
                                       name="userid" readonly placeholder="ID" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="indexPass" class="col-sm-2 control-label ">原始密码</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control " id="indexPass" name="indexPass"
                                       placeholder="请输入旧密码">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="indexPass1" class="col-sm-2 control-label">密码</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="indexPass1" name="indexPass1"
                                       placeholder="请输入新密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="indexPass2" class="col-sm-2 control-label">确认密码</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="indexPass2" name="indexPass2"
                                       placeholder="请确认新密码">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i
                            class="fa fa-close button-in-i"></i>取消
                    </button>
                    <button type="button" class="btn btn-primary" id="indexUpdateBtn"><i
                            class="fa fa-check button-in-i"></i>保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>