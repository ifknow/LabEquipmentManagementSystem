<#--freemark方式引入绝对路径-->
<#assign ctx=springMacroRequestContext.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>实验室列表</title>
    <#--引入js-->
    <script src="${ctx}/js/laboratoryList.js" type="text/javascript"/>
</head>
<body>
<!-- content start -->
<section class="content">
    <!-- 分类列表 -->
    <div class="row">
        <div class="col-xs-12">
            <!-- search-form start -->
            <section class="search-form hidden-print" id="search" style="display: block">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="labName" name="labName" class="form-control"
                                           style="width: 100%;" placeholder="请输入实验室名称">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="fzrName" name="fzrName" class="form-control"
                                           style="width: 100%;" placeholder="请输入负责人姓名">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-4">
                                <button id="queryBtn" class="btn btn-primary my-btn"><i
                                            class="fa fa-search button-in-i"></i>查询
                                </button>
                                <button id="clearBtn" class="btn btn-default my-btn"><i
                                            class="fa fa-trash button-in-i"></i>清空
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- search-form end -->
            <!-- result-table start -->
            <section class="result-table">
                <div class="box">
                    <div class="box-header table-top-button">
                        <div class="col-xs-1">

                            <button type="submit" class="btn btn-info my-btn" id="addNewBtn"><i
                                        class="fa fa-plus button-in-i"></i> 新增实验室
                            </button>
                        </div>
                    </div>
                    <div class="grid-table">
                        <!--startprint1-->
                        <table id="grid-table" class="table-striped table-hover"></table>
                        <!--endprint1-->
                        <div id="grid-pager" class="box-footer"></div>
                    </div>
                </div>
            </section>
            <!-- result-table end -->
        </div>
    </div>
</section>
<!-- content end -->
<!--    编辑  Modal -->
<div class="modal fade bs-example-modal-lg" id="editmyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="Eidtform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle1">实验室信息编辑</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="idLable">
                            <label for="id" class="col-sm-3 control-label">ID</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="id" placeholder="实验室ID" name="id" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">实验室名称</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="name" placeholder="请输入实验室名称"
                                       name="name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fzr" class="col-sm-3 control-label">负责人</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="fzr" name="fzr"
                                       placeholder="请输入实验室负责人"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fzrDh" class="col-sm-3 control-label">负责人电话</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="fzrDh"
                                       name="fzrDh" placeholder="请输入实验室负责人电话"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-3 control-label">具体地点</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="address"
                                       name="address" placeholder="请输入实验室具体地点"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bz" class="col-sm-3 control-label">实验室备注</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="bz"
                                       name="bz" placeholder="请输入实验室备注"/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-close button-in-i"></i>取消
                        </button>
                        <button type="button" class="btn btn-primary" id="saveBtn"><i
                                    class="fa fa-check button-in-i"></i>保存
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>