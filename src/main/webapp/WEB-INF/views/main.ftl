<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>实验室设备管理系统</title>
    <!-- 百度地图 -->
<#-- <script type="text/javascript"
         src="https://api.map.baidu.com/api?v=3.0&ak=GFiYNQ9adSaq6TeCHRFeiWGqV8Ykww6k"></script>
 <!-- 高德地图 &ndash;&gt;
 <script type="text/javascript"
         src="http://webapi.amap.com/maps?v=1.3&key=1d04fb280460062c676179ed18fb1f5a"></script>-->
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../user/dist/img/zimuz.jpg" rel="shortcut icon"/>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../user/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../user/plugins/fontawesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../user/plugins/ionicons/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../user/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../user/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../user/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="../user/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap datetimepicker -->
    <link rel="stylesheet" href="../user/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="../user/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../user/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../user/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../user/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../assets/js/zTree/css/zTreeStyle/zTreeStyle.css">
    <link rel="stylesheet" href="../assets/css/jquery.fileupload-ui.css"/>
    <link rel="stylesheet" href="../assets/css/jquery.fileupload.css"/>
    <link rel="stylesheet" href="../user/dist/css/common.css">
    <!-- jQuery 2.2.3 -->
    <script src="../user/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="../assets/js/jquery-ui-1.10.3.full.min.js"></script>
    <script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="../assets/js/jquery.slimscroll.min.js"></script>
    <script src="../assets/js/jquery.sparkline.min.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>
    <script src="../assets/js/date-time/moment.min.js"></script>
    <script src="../assets/js/flot/jquery.flot.min.js"></script>
    <script src="../assets/js/flot/jquery.flot.pie.min.js"></script>
    <script src="../assets/js/flot/jquery.flot.resize.min.js"></script>
    <script src="../assets/js/chosen.jquery.min.js"></script>
    <script src="../assets/js/fuelux/fuelux.spinner.min.js"></script>
    <script src="../assets/js/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="../assets/js/jqGrid/i18n/grid.locale-en.js"></script>
    <script src="../assets/js/bootbox.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../user/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../user/dist/js/app.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../user/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- Select2 -->
    <script src="../user/plugins/select2/select2.full.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../user/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- date-range-picker -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
    <script src="../user/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datetimepicker -->
    <script src="../user/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="../user/plugins/datetimepicker/bootstrap-datetimepicker.zh-CN.js"></script>
    <!-- bootstrap time picker -->
    <script src="../user/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- ztree -->
    <script src="../assets/js/zTree/js/jquery.ztree.core-3.5.js"></script>
    <script src="../assets/js/zTree/js/jquery.ztree.excheck-3.5.js"></script>
    <script src="../assets/js/zTree/js/jquery.ztree.exedit-3.5.min.js"></script>
    <!-- DataTables -->
    <!-- qrcode -->
    <script src="../user/plugins/qrcode/jquery.qrcode.min.js"></script>
    <script src="../user/plugins/qrcode/jquery.qrcode.js"></script>
    <script src="../user/plugins/qrcode/qrcode.js"></script>

    <script src="../assets/js/upload/vendor/jquery.ui.widget.js"></script>
    <script src="../assets/js/upload/vendor/load-image.all.min.js"></script>
    <script src="../assets/js/upload/vendor/canvas-to-blob.min.js"></script>
    <script src="../assets/js/upload/jquery.iframe-transport.js"></script>
    <script src="../assets/js/upload/jquery.fileupload.js"></script>
    <script src="../assets/js/upload/jquery.fileupload-process.js"></script>
    <script src="../assets/js/upload/jquery.fileupload-image.js"></script>
    <script src="../assets/js/upload/cors/jquery.xdr-transport.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="../assets/js/upload/jquery.fileupload-validate.js"></script>
    <script src="../user/dist/js/index.js"></script>
    <script src="../user/dist/js/common.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div id="loading" class="box box-solid hide">
    <div class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>
<!-- wrapper start -->
<div class="wrapper">
    <!-- header start -->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini">
		      	<img src="../user/dist/img/zimuz.jpg" width="40px" height="40px">
		      </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">实验室设备管理系统</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs" id="quanXianName">登陆用户</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <p>
                                    <small id="loginUserName">###</small>
                                </p>
                                <p>
                                    <small id="loginTime">###</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat" onclick="loginOut()">退 出</a>
                                </div>
                               <#-- <div class="pull-right" style="margin-right:10px;">
                                    <button id="editPassword" class="btn btn-default btn-flat">修改密码</button>
                                </div>-->
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
            <!-- <div class="breadcrumbs" id="breadcrumbs" style="margin-left:0px;margin-top:50px;">
             <ul class="breadcrumb">
                 <li>
                     <i class=""></i>
                     <a href="index.html">&nbsp;首页</a>
                 </li>
                 <li class="active">控制台</li>
             </ul>
         </div> -->
        </nav>
    </header>
    <!-- header end -->
    <!-- sidebar start -->
    <aside class="main-sidebar">
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header"></li>
                <li class="treeview" id="tree01">
                    <a href="#">
                        <i class="fa fa-cubes"></i>
                        <span>通知管理</span>
                        <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                    </a>
                    <ul class="treeview-menu hover-treeview-menu">
                        <li id="moudle11"><a href="javascript:void(0);"
                                             onclick="loadPage('../InformManagement/toInformManagement');">
                            <i class="fa fa-circle-o"></i> 通知管理 </a></li>
                    </ul>
                </li>
                <li class="treeview" id="tree02">
                    <a href="#">
                        <i class="fa fa-money"></i> <span>用户管理</span>
                        <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                    </a>
                    <ul class="treeview-menu hover-treeview-menu">
                        <li id="moudle21"><a href="#" onclick="loadPage('../userList/toAdminList');">
                            <i class="fa fa-circle-o"></i> 管理员列表 </a></li>
                        <li id="moudle22"><a href="#" onclick="loadPage('../userList/toUserList');">
                            <i class="fa fa-circle-o"></i> 用户列表 </a></li>
                    </ul>
                </li>
                <li class="treeview" id="tree03">
                    <a href="#">
                        <i class="fa fa-calculator"></i>
                        <span>实验室管理</span>
                        <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                    </a>
                    <ul class="treeview-menu hover-treeview-menu">
                        <li id="moudle31"><a href="#" onclick="loadPage('../labList/toLaboratoryList');">
                            <i class="fa fa-circle-o"></i> 实验室管理 </a></li>
                        <li id="moudle32"><a href="javascript:void(0);"
                                             onclick="loadPage('../labAppointment/toLaboratoryAppointment');">
                            <i class="fa fa-circle-o"></i> 实验室预约</a></li>
                        <li id="moudle33"><a href="javascript:void(0);"
                                             onclick="loadPage('../LaboratoryAppointmentLog/skiptoLaboratoryAppointmentLogPage');">
                            <i class="fa fa-circle-o"></i> 实验室预约记录</a></li>
                    </ul>
                </li>
                <li class="treeview" id="tree04">
                    <a href="#">
                        <i class="fa fa-laptop"></i> <span>设备管理</span>
                        <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                    </a>
                    <ul class="treeview-menu hover-treeview-menu">
                        <li id="moudle41"><a href="javascript:void(0);"
                                             onclick="loadPage('../tecParamManagement/toTecParamManagement');"><i
                                class="fa fa-circle-o"></i> 设备管理 </a></li>
                    </ul>
                </li>
                <li class="treeview" id="tree06">
                    <a href="#">
                        <i class="fa fa-bookmark"></i> <span>设备借用管理</span>
                        <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                    </a>
                    <ul class="treeview-menu hover-treeview-menu">
                        <li id="moudle61"><a href="javascript:void(0);"
                                             onclick="loadPage('../equipmentBorrowManagement/toEquipmentBorrowManagement');"><i
                                        class="fa fa-circle-o"></i> 设备借用管理 </a></li>
                        <li id="moudle62"><a href="javascript:void(0);"
                                             onclick="loadPage('../equipmentBorrowLog/toEquipmentBorrowLog');"><i
                                        class="fa fa-circle-o"></i> 设备借用日志 </a></li>
                    </ul>
                </li>
                <li class="treeview" id="tree05">
                    <a href="#">
                        <i class="fa fa-server"></i> <span>设备维修管理</span>
                        <span class="pull-right-container">
		              <i class="fa fa-angle-left pull-right"></i>
		            </span>
                    </a>
                    <ul class="treeview-menu hover-treeview-menu">
                        <li id="moudle51"><a href="javascript:void(0);"
                                             onclick="loadPage('../equipmentMaintenance/toEquipmentMaintenance');"><i
                                class="fa fa-circle-o"></i> 设备维修管理 </a></li>
                        <li id="moudle52"><a href="javascript:void(0);"
                                             onclick="loadPage('../equipmentRepairLog/toEquipmentRepairLogInfo');"><i
                                class="fa fa-circle-o"></i> 设备报修日志 </a></li>
                    </ul>
                </li>

            </ul>
        </section>
    </aside>
    <!-- sidebar end -->

    <!-- content start -->
    <section class="content-wrapper">
        <div id="indexHtml">
            <script type="text/javascript">
                $("#indexHtml").load("../Welcome/toWelcomePage");
            </script>
        </div>
    </section>
    <!-- content end -->
</div>
<!-- wrapper end -->
</body>
</html>
