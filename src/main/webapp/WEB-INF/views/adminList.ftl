<#--freemark方式引入绝对路径-->
<#assign ctx=springMacroRequestContext.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户列表</title>
    <#--引入js-->
    <script src="${ctx}/js/adminList.js" type="text/javascript"/>
</head>
<body>
<!-- content start -->
<section class="content">
    <!-- 分类列表 -->
    <div class="row">
        <div class="col-xs-12">
            <!-- search-form start -->
            <section class="search-form hidden-print" id="search" style="display: block">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="phone1" name="phone1" class="form-control"
                                           style="width: 100%;" placeholder="请输入手机号码">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-2">
                                <div class="form-group">
                                    <input id="realname1" name="realname1" class="form-control"
                                           style="width: 100%;" placeholder="请输入真实姓名">
                                </div>
                            </div>
                            <div class="col-sm-2 col-lg-4">
                                <button id="queryBtn" class="btn btn-primary my-btn"><i
                                            class="fa fa-search button-in-i"></i>查询
                                </button>
                                <button id="clearBtn" class="btn btn-default my-btn"><i
                                            class="fa fa-trash button-in-i"></i>清空
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- search-form end -->
            <!-- result-table start -->
            <section class="result-table">
                <div class="box">
                    <div class="box-header table-top-button" id="addNewBtnMessage">
                        <div class="col-xs-1">

                            <button type="submit" class="btn btn-info my-btn" id="addNewBtn"><i
                                        class="fa fa-plus button-in-i"></i> 新增管理员
                            </button>
                        </div>
                    </div>
                    <div class="grid-table">
                        <!--startprint1-->
                        <table id="grid-table" class="table-striped table-hover"></table>
                        <!--endprint1-->
                        <div id="grid-pager" class="box-footer"></div>
                    </div>
                </div>
            </section>
            <!-- result-table end -->
        </div>
    </div>
</section>
<!-- content end -->
<!--    编辑  Modal -->
<div class="modal fade bs-example-modal-lg" id="editmyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="Eidtform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle1">用户信息编辑</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="idLable">
                            <label for="id" class="col-sm-2 control-label">ID</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="id" placeholder="ID" name="id" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">移动电话</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="phone" name="phone"
                                       placeholder="请输入移动电话"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="realname" class="col-sm-2 control-label">真实姓名</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="realname" name="realname"
                                       placeholder="请输入您的真实姓名"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">住址</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="address" name="address"
                                       placeholder="请输入您的住址"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="remark" class="col-sm-2 control-label">备注</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="remark" name="remark"
                                       placeholder="请输入您的备注"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rolename" class="col-sm-2 control-label">权限</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="rolename"
                                       name="rolename" placeholder="请输入权限" disabled readonly/>
                            </div>
                        </div>
                        <#--<div class="form-group">
                            <label for="Etime" class="col-sm-2 control-label">注册时间</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="Etime"
                                       name="Etime"  placeholder="注册时间"/>
                            </div>
                        </div>-->
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-close button-in-i"></i>取消
                        </button>
                        <button type="button" class="btn btn-primary" id="saveBtn"><i
                                    class="fa fa-check button-in-i"></i>保存
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<#--修改密码-->
<div class="modal fade bs-example-modal-lg" id="indexEditP" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="editPasswordForm" role="form" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="indexModeltitle">修改密码</h4>
                </div>

                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <input type="hidden" id="indexId" name="indexId" value="0"/>
                        <div class="form-group" hidden>
                            <label for="indexMechanismCode" class="col-sm-2 control-label">ID</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="userid"
                                       name="userid" readonly placeholder="ID" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="indexPass" class="col-sm-2 control-label ">原始密码</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control " id="indexPass" name="indexPass"
                                       placeholder="请输入旧密码">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="indexPass1" class="col-sm-2 control-label">密码</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="indexPass1" name="indexPass1"
                                       placeholder="请输入新密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="indexPass2" class="col-sm-2 control-label">确认密码</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="indexPass2" name="indexPass2"
                                       placeholder="请确认新密码">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                class="fa fa-close button-in-i"></i>取消
                    </button>
                    <button type="button" class="btn btn-primary" id="indexUpdateBtn"><i
                                class="fa fa-check button-in-i"></i>保存
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>