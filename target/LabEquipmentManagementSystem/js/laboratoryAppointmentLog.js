var grid;
var form;
var form1;
$(function () {

    $(".select2").select2();

    var pageNo = 1;
    var pageSize = 10;
    var grid_selector = "#grid-table";
    var pager_selector = "#grid-pager";
    //初始化数据表格
    grid = $(grid_selector).jqGrid({
        datatype: "json",
        url: '../LaboratoryAppointmentLog/getAllLabLogList',
        mtype: 'GET',
        page: pageNo,
        postData: {
            "name": $("#labName").val(),
            "fzr": $("#fzrName").val(),
        },
        height: 380,
        width: 300,
        colNames: ['实验室名称', "负责人", "预约时间", "返还时间", "预约人"],
        colModel: [
            {name: 'name', index: 'name', sortable: false, align: 'center', width: 200},
            {name: 'fzr', index: 'fzr', sortable: false, align: 'center', width: 200},
            {name: 'timestr', index: 'timestr', sortable: false, align: 'center', width: 200},
            {name: 'endtimestr', index: 'endtimestr', sortable: false, align: 'center', width: 200},
            {name: 'realname', index: 'realname', sortable: false, align: 'center', width: 200},
        ],
        viewrecords: true,
        rowNum: pageSize,
        rownumbers: true,//添加左侧行号
        rowList: [10, 15, 20],
        pager: pager_selector,
        altRows: true,
        multiselect: false,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
            }, 0);
            var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);
            /*var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);*/
        },
        autowidth: true,
        jsonReader: jsonReader,   // json中代表实际模型数据的入口
        rownumWidth: 55,
        prmNames: prmNames,
        recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
        emptyrecords: "没有记录",
        loadtext: "正在加载...",
        pgtext: "第 {0} 页  ,共 {1} 页"
    });
    //初始化分页条
    showNavGrid(grid_selector, pager_selector);
    //初始化信息校验表单
    form = $("#addform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    form1 = $("#Editform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });

    //查询
    $("#queryBtn").click(function () {
        var filters = {
            "name": $("#labName").val(),
            "fzr": $("#fzrName").val(),
        }
        grid.jqGrid("setGridParam", {postData: filters, page: 1});
        grid.trigger('reloadGrid');
    });
    //清空
    $("#clearBtn").click(function () {
        $("#labName").val("");
        $("#fzrName").val("");
    });

});

//打开预约
function openEdit(id) {
    bootbox.confirm("您确定要预约该实验室吗？", function (result) {
        var userId = localStorage.getItem("userId");
        //Ajax请求查询编辑的信息
        $.post("../labAppointment/appointment", {
            "id": id,
            "userId": userId,
            "_method": "PUT"
        }, function (data) {
            if (data.resultCode = 201) {
                bootbox.alert(data.resultMessage);
            } else if (data.resultCode = 200) {
                bootbox.alert(data.resultMessage);
            }
            grid.trigger('reloadGrid');
        })
    });
}

//清除表单
function clearForm() {
    //清空内容
    $("#id").val("");
    $("#name").val("");
    $("#fzr").val("");
    $("#fzrDh").val("");
    $("#address").val("");
    $("#bz").val("");
    //清除验证状态
    $("#addform .form-group").removeClass('has-info').removeClass('has-error');
}

