var grid;
var form;
var form1;
$(function () {

    $(".select2").select2();

    var pageNo = 1;
    var pageSize = 10;
    var grid_selector = "#grid-table";
    var pager_selector = "#grid-pager";
    //初始化数据表格
    grid = $(grid_selector).jqGrid({
        datatype: "json",
        url: '../equipmentBorrowManagement/getAllEquipmentBorrowInfo',
        mtype: 'GET',
        page: pageNo,
        postData: {
            "xh": $("#equipmentXH").val(),
            "zzs": $("#equipmentZZS").val()
        },
        height: 380,
        width: 300,
        colNames: ['设备型号', "设备制造商", "所属实验室", "实验室地址", "是否借用", "借用人", "操作"],
        colModel: [
            {name: 'xh', index: 'xh', sortable: false, align: 'center', width: 100},
            {name: 'zzs', index: 'zzs', sortable: false, align: 'center', width: 120},
            {name: 'name', index: 'name', sortable: false, align: 'center', width: 100},
            {name: 'address', index: 'address', sortable: false, align: 'center', width: 120},
            {
                name: 'isJy', index: 'isJy', width: 120, fixed: true, sortable: false, resize: false,
                formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == 0) {
                        return "<button class='btn btn-default btn-xs'>未借用</button>";
                    } else {
                        return "<button class='btn btn-primary btn-xs'>已借用</button>";
                    }
                }
            },
            {name: 'realname', index: 'realname', sortable: false, align: 'center', width: 120},
            {
                name: 'isJy', index: 'isJy', width: 120, fixed: true, sortable: false, resize: false,
                formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == 0) {
                        return "<button class='btn btn-primary btn-xs' onclick='openEdit(" + rowObject.isJy + "," + rowObject.id + ")' ><i class='fa fa-edit button-in-i'></i>借用</button>";
                    } else {
                        return "<button class='btn btn-default btn-xs' onclick='openEdit(" + rowObject.isJy + "," + rowObject.id + ")' ><i class='fa fa-edit button-in-i'></i>归还</button>";
                    }
                }
            },
        ],
        viewrecords: true,
        rowNum: pageSize,
        rownumbers: true,//添加左侧行号
        rowList: [10, 15, 20],
        pager: pager_selector,
        altRows: true,
        multiselect: false,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
            }, 0);
            var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);
            /*var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);*/
        },
        autowidth: true,
        jsonReader: jsonReader,   // json中代表实际模型数据的入口
        rownumWidth: 55,
        prmNames: prmNames,
        recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
        emptyrecords: "没有记录",
        loadtext: "正在加载...",
        pgtext: "第 {0} 页  ,共 {1} 页"
    });
    //初始化分页条
    showNavGrid(grid_selector, pager_selector);
    //初始化信息校验表单
    form = $("#addform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    form1 = $("#Editform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });

    //查询
    $("#queryBtn").click(function () {
        var filters = {
            "xh": $("#equipmentXH").val(),
            "zzs": $("#equipmentZZS").val()
        }
        grid.jqGrid("setGridParam", {postData: filters, page: 1});
        grid.trigger('reloadGrid');
    });
    //清空
    $("#clearBtn").click(function () {
        $("#equipmentXH").val("");
        $("#equipmentZZS").val("");
    });

    //保存
    $("#saveBtn").click(function () {
        doSubmit();
    });

});

//打开编辑
function openEdit(isJy, id) {
    //发送ajax请求查询该设备是否报修
    if (isJy == 1) {
        //如果已经借用了，则发送请求返还
        bootbox.confirm("您确定要归还该设备吗？", function (result) {
            if (result) {
                var userId = localStorage.getItem("userId");
                //Ajax请求查询编辑的信息
                $.post("../equipmentBorrowManagement/backEquipment", {
                    "id": id,
                    "userId": userId,
                    "_method": "PUT"
                }, function (data) {
                    if (data.resultCode = 201) {
                        bootbox.alert(data.resultMessage);
                    } else if (data.resultCode = 200) {
                        bootbox.alert(data.resultMessage);
                    }
                    grid.trigger('reloadGrid');
                })
            }
        });
    } else {
        //如果未借用了，则发送请求借用
        bootbox.confirm("您确定要借用该设备吗？", function (result) {
            if (result) {
                var userId = localStorage.getItem("userId");
                //Ajax请求查询编辑的信息
                $.post("../equipmentBorrowManagement/backEquipment", {
                    "id": id,
                    "userId": userId,
                    "_method": "PUT"
                }, function (data) {
                    if (data.resultCode = 201) {
                        bootbox.alert(data.resultMessage);
                    } else if (data.resultCode = 200) {
                        bootbox.alert(data.resultMessage);
                    }
                    grid.trigger('reloadGrid');
                })
            }
        });
    }

}

//保存编辑后的信息
function doSubmit() {
    $("#loading").removeClass('hide').addClass('show');
    $.post("../equipmentMaintenance/addBxInfo?_method=POST", $('#Eidtform').serialize(), function (data) {
        if (data.resultCode == 201) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
            grid.trigger('reloadGrid');
        } else if (data.resultCode == 200) {
            $("#loading").removeClass('show').addClass('hide');
            bootbox.alert(data.resultMessage);
            grid.trigger('reloadGrid');
            $('#editmyModal').modal('hide');
        }
    });
}

//清除表单
function clearForm() {
    //清空内容
    $("#id").val("");
    $("#title").val("");
    $("#bz").val("");
    $("#wz").val("");

    //清除验证状态
    $("#addform .form-group").removeClass('has-info').removeClass('has-error');
}

