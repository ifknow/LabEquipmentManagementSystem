$(function () {

    $("#comfirmUserPassword").blur(function () {
        if ($("#comfirmUserPassword").val() == "") {
            bootbox.alert("确认密码不能为空！");
            return;
        }
        if ($("#userPassword").val() != $("#comfirmUserPassword").val()) {
            bootbox.alert("两次密码不相同！");
            return;
        }
    });

    $("#userAccount").blur(function () {
        //发送请求验证手机号是否重复
        $.ajax({
            url: "register/verify",
            type: "POST",
            // dataType:'json',
            data: {
                "userAccount": $('#userAccount').val()
            },
            success: function (data) {
                if (data.resultCode == 200) {
                    $("#validateMessage").val("1");
                } else {
                    $("#validateMessage").val("0");
                    bootbox.alert(data.resultMessage);
                }

            }
        });
    });

});

/**
 * 注册验证
 */
function register() {
    //验证手机号非空
    if ($("#userAccount").val() == "") {
        bootbox.alert("手机号码不能为空！")
        return;
    }
    //验证手机号格式
    if (isPhoneNo($.trim($("#userAccount").val())) == false) {
        bootbox.alert("手机号码格式不正确！")
        return;
    }
    //验证真实姓名
    if ($("#realName").val() == "") {
        bootbox.alert("真实姓名不能为空！")
        return;
    }
    //验证所在学院
    if ($("#xy").val() == "") {
        bootbox.alert("所在学院不能为空！");
        return;
    }
    //验证所在学院
    if ($("#bj").val() == "") {
        bootbox.alert("所在班级不能为空！");
        return;
    }
    //验证学号/工号
    if ($("#number").val() == "") {
        bootbox.alert("学号/工号不能为空！");
        return;
    }
    //验证地址
    if ($("#address").val() == "") {
        bootbox.alert("地址不能为空！");
        return;
    }
    //验证密码
    if ($("#userPassword").val() == "") {
        bootbox.alert("密码不能为空！");
        return;
    }
    valdatePass();
    //验证角色
    if ($("#roleId option:selected").val() == 0) {
        bootbox.alert("请选择角色！");
        return;
    }
    if ($("#validateMessage").val() == "0") {
        bootbox.alert("手机号码已注册！");
        return;
    }

    //注册
    $.ajax({
        url: "register/register",
        type: "POST",
        // dataType:'json',
        data: {
            "phone": $('#userAccount').val(),
            "realname": $("#realName").val(),
            "academy": $("#xy").val(),
            "classs": $("#bj").val(),
            "number": $("#number").val(),
            "address": $("#address").val(),
            "password": $("#userPassword").val(),
            "roleid": $("#roleId option:selected").val()
        },
        success: function (data) {
            if (data.resultCode == 200) {
                bootbox.confirm(data.resultMessage + "点击确认返回登录页面", function (result) {
                    if (result) {
                        window.location.href = "login.ftl";
                    }
                });
            } else {
                bootbox.alert(data.resultMessage);
            }

        }
    });


}

// 验证手机号
function isPhoneNo(phone) {
    var pattern = /^1[34578]\d{9}$/;
    return pattern.test(phone);
}

function valdatePass() {
    $("#comfirmUserPassword").blur(function () {
        if ($("#comfirmUserPassword").val() == "") {
            bootbox.alert("确认密码不能为空！");
            return;
        }
        if ($("#userPassword").val() != $("#comfirmUserPassword").val()) {
            bootbox.alert("两次密码不相同！");
            return;
        }
    });
}