$(function () {

    $(".select2").select2();

    //清空
    $("#clearBtn").click(function () {
        $("#realname").val("");
        $("#number").val("");
        $("#phone").val("");
        $("#academy").val("");
        $("#classs").val("");
        $("#password").val("");
        $("#role").val(null).trigger("change");//清除品牌
    });

    //返回
    $("#back").click(function () {
        toUserList();
    });

    //保存
    $("#saveBtn").click(function () {
        save();
    });
});

//返回到主页
function toUserList() {
    loadPage("../userList/skipToUserList");
}

//保存
function save() {
    //验证非空
    if ($("#realname").val() == "") {
        bootbox.alert("请输入您的真实姓名！");
        return;
    }
    if ($("#number").val() == "") {
        bootbox.alert("请输入您的学号/工号！");
        return;
    }
    if ($("#phone").val() == "") {
        bootbox.alert("请输入您的手机号码！");
        return;
    }
    //验证格式
    if (isPhoneNo($.trim($("#phone").val())) == false) {
        bootbox.alert("手机号码格式不正确！")
        return;
    }
    if ($("#academy").val() == "") {
        bootbox.alert("请输入您所在的学院！");
        return;
    }
    if ($("#classs").val() == "") {
        bootbox.alert("请输入您所在的班级！");
        return;
    }
    if ($("#password").val() == "") {
        bootbox.alert("请设置您要登录的密码！");
        return;
    }
    if ($("#role option:selected").val() == 0) {
        bootbox.alert("请选择角色！");
        return;
    }
    if ($("#msg").val() == "不可用") {
        bootbox.alert("该手机号已经被注册过了！")
        return;
    }
    //发送ajax请求
    $.post("../userList/addUser", {
        "realname": $("#realname").val(),
        "stunumber": $("#number").val(),
        "phone": $("#phone").val(),
        "academy": $("#academy").val(),
        "classs": $("#classs").val(),
        "address": $("#address").val(),
        "remark": $("#remark").val(),
        "rolename": $("#role option:selected").val(),
        "password": $("#password").val()
    }, function (data) {
        if (data.resultCode == 200) {
            //如果添加成功，跳转页面
            bootbox.alert("用户信息添加成功！");
            loadPage("../userList/skipToUserList")
        } else {
            //添加失败时，不进行跳转
            bootbox.alert("用户信息添加失败！");
        }

    });
}

$("#phone").blur(function () {
    validatePhone();
});
$("#password1").blur(function () {
    validatePass();
});

function validatePass() {
    if ($("#password").val() != $("#password1").val()) {
        $("#passMsg").html("两次密码不一致！");
    }else {
        $("#passMsg").html("密码一致！");
    }
}

// 验证手机号
function isPhoneNo(phone) {
    var pattern = /^1[34578]\d{9}$/;
    return pattern.test(phone);
}

function validatePhone() {
    //验证手机号格式
    if (isPhoneNo($.trim($("#phone").val())) == false) {
        $("#msg").html("手机号码格式不正确");
        return;
    }

    //验证手机号是否存在
    $.get("../userList/validatePhone", {
        "phone": $("#phone").val(),
    }, function (data) {
        if (data.resultCode == 200) {
            $("#msg").html("可用");
        } else {
            $("#msg").html("不可用");
        }

    });
}
