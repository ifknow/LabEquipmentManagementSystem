var grid;
var form;
var form1;
$(function () {

    $(".select2").select2();

    var flag = false;
    //分配权限
    if (localStorage.getItem("roleId") == 1) {
        //管理员具有所有权限，不做任何操作
    } else {
        //教师、学生具有的权限基本相同、包括通知的查看、基本信息的查看、编辑、修改密码
        flag = true;
    }

    var pageNo = 1;
    var pageSize = 10;
    var grid_selector = "#grid-table";
    var pager_selector = "#grid-pager";
    //初始化数据表格
    grid = $(grid_selector).jqGrid({
        datatype: "json",
        url: '../InformManagementTrash/getAllTrashNotice',
        mtype: 'GET',
        page: pageNo,
        postData: {
            "name": $("#searchTitle").val(),
        },
        height: 380,
        width: 300,
        colNames: ['标题', '通知内容', '添加时间', "操作"],
        colModel: [
            {name: 'name', index: 'name', sortable: false, align: 'center', width: 150},
            {name: 'content', index: 'content', sortable: false, align: 'center', width: 400},
            {name: 'timeStr', index: 'timeStr', sortable: false, align: 'center', width: 200},
            {
                name: 'id', index: 'id', fixed: true, sortable: false, resize: false, width: 200,
                formatter: function (cellvalue, options, rowObject) {
                    if (flag != true)
                        return "<button class='btn btn-primary btn-xs' onclick='openShow(\"" + rowObject.id + "\")'><i class='fa fa-reddit button-in-i'></i>查看</button>"
                            + "<button class='btn btn-danger btn-xs' onclick='returnPage(\"" + rowObject.id + "\")' ><i class='fa fa-backward button-in-i'></i>撤回</button>";
                    else
                        return "<button class='btn btn-primary btn-xs' onclick='openShow(\"" + rowObject.id + "\")'><i class='fa fa-reddit button-in-i'></i>查看</button>";
                }
            },
        ],
        viewrecords: true,
        rowNum: pageSize,
        rownumbers: true,//添加左侧行号
        rowList: [10, 15, 20],
        pager: pager_selector,
        altRows: true,
        multiselect: false,
        loadComplete: function (data) {
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
            }, 0);
            var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);
            /*var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);*/
        },
        autowidth: true,
        jsonReader: jsonReader,   // json中代表实际模型数据的入口
        rownumWidth: 55,
        prmNames: prmNames,
        recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
        emptyrecords: "没有记录",
        loadtext: "正在加载...",
        pgtext: "第 {0} 页  ,共 {1} 页"
    });
    //初始化分页条
    showNavGrid(grid_selector, pager_selector);
    //初始化信息校验表单
    form = $("#addform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    form1 = $("#Editform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    //查询
    $("#queryBtn").click(function () {
        var filters = {
            "name": $("#searchTitle").val(),
        }
        grid.jqGrid("setGridParam", {postData: filters, page: 1});
        grid.trigger('reloadGrid');
    });
    //清空
    $("#clearBtn").click(function () {
        $("#searchTitle").val("");
    });
    //返回
    $("#goBack").click(function () {
        goBack();
    });
});

function goBack() {
    loadPage("../InformManagement/toInformManagement")
}

//打开查看表单
function openShow(id) {
    clearForm();
    $("#showmyModal").modal();
    $("#modeltitle").html("通知详情");
    $("#idLable").css("display", "none");
    //Ajax请求查询编辑的信息
    var ware = $("#warehouseCodeEidt");
    $.post("../InformManagement/getAllNoticeById", {
        "id": id,
        "_method": "GET",
    }, function (data) {
        $("#noticeId").val(data[0].id);
        $("#noteTitle").val(data[0].name);
        $("#noteContent").val(data[0].content);
    })
}

//撤回删除通知
function returnPage(str) {
    array = str.split(",");
    bootbox.confirm("您确定要撤回删除该通知吗?", function (result) {
        if (result) {
            $.post("../InformManagementTrash/cancelDeleteNoticeById", {
                "id": str,
                "_method": "PUT",
            }, function (data) {
                if (data.resultCode = 200) {
                    bootbox.alert("撤回成功！");
                } else {
                    bootbox.alert("撤回失败！");
                }
                grid.trigger('reloadGrid');
            });
        }
    });
}

//导出数据为pdf
function exportData(obj) {
    if (obj < 10) {
        bdhtml = window.document.body.innerHTML;//获取当前页的html代码
        sprnstr = "<!--startprint" + obj + "-->";//设置打印开始区域
        eprnstr = "<!--endprint" + obj + "-->";//设置打印结束区域
        prnhtml = bdhtml.substring(bdhtml.indexOf(sprnstr) + 25); //从开始代码向后取html
        prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));//从结束代码向前取html
        window.document.body.innerHTML = prnhtml;
        window.print();
        window.document.body.innerHTML = bdhtml;
    } else {
        window.print();
    }
}

//导出excel
function exportDataExcel(ids) {
    bootbox.confirm("确定导出数据为EXCEL吗？O(∩_∩)O哈哈~", function (result) {
        if (result) {
            location.href = ("../goodsInfo/exportExcel?ids=" + ids);
        }
    });
}

//清除表单
function clearForm() {
    //清空内容
    $("#id").val("");
    $("#content").val("");
    $("#name").val("");
    $("#noticeId").val("");
    $("#noteTitle").val("");
    $("#noteContent").val("");


    //清除验证状态
    $("#addform .form-group").removeClass('has-info').removeClass('has-error');
}