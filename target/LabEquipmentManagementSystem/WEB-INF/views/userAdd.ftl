<#--freemark方式引入绝对路径-->
<#assign ctx=springMacroRequestContext.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>新建普通用户</title>
    <#--引入js-->
    <script src="${ctx}/js/userAdd.js" type="text/javascript"/>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard">首页</i></li>
        <li>用户列表</li>
        <li>新建用户</li>
    </ol>
    <div class="clearfix"></div>
</section>
<!-- content start -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- goods-form start -->
            <form class="form-horizontal" role="form" id="goodsForm">
                <div class="form-group">
                    <label for="realname" class="col-sm-2 control-label">真实姓名：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="realname" name="realname" class="form-control"
                               placeholder="请输入您的真实姓名"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="number" class="col-sm-2 control-label">学号/工号：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="number" name="number" class="form-control"
                               placeholder="请输入您的学号/工号"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">手机号码：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="phone" name="phone" class="form-control"
                               placeholder="请输入您的手机号码"/>
                        <span id="msg" style="color: red"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="academy" class="col-sm-2 control-label">学院名称：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="academy" name="academy" class="form-control"
                               placeholder="请输入所在学院名称"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="number" class="col-sm-2 control-label">班级：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="classs" name="classs" class="form-control"
                               placeholder="请输入您的班级"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="number" class="col-sm-2 control-label">密码：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="password" name="password" type="password" class="form-control"
                               placeholder="请输入您的密码"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password1" class="col-sm-2 control-label">确认密码：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="password1" name="password1"type="password" class="form-control"
                               placeholder="请确认您的密码"/>
                        <span id="passMsg" style="color: #red;"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="role" class="col-sm-2 control-label">角色：</label>
                    <div class="col-sm-6 col-lg-3">
                        <select id="role" name="role" class="form-control select2"
                                style="width: 100%;" data-placeholder="--/ 请选择角色 /--">
<#--                            <option value="0">--/ 请选择角色 /--</option>-->
                            <option value="3"> 学生</option>
<#--                            <option value="2"> 教师</option>-->
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">地址：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="address" name="address" class="form-control"
                               placeholder="请输入您的地址"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="remark" class="col-sm-2 control-label">备注：</label>
                    <div class="col-sm-6 col-lg-3">
                        <input id="remark" name="remark" class="form-control"
                               placeholder="请输入备注"/>
                    </div>
                </div>
            </form>
        </div>
        <#--规格添加结束-->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-2 col-lg-1">
                <button type="button" class="btn btn-primary btn-block" id="saveBtn">保存</button>
            </div>
            <div class="col-sm-2 col-lg-1">
                <button type="button" class="btn btn-warning btn-block" id="clearBtn">清空</button>
            </div>
            <div class="col-sm-2 col-lg-1">
                <button class="btn btn-default btn-block" id="back">返回</button>
            </div>
        </div>
        <!-- goods-add-form end -->
    </div>
</section>
<!-- content end -->
</body>
</html>