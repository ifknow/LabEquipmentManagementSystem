<#--freemark方式引入绝对路径-->
<#assign ctx=springMacroRequestContext.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>通知管理页面</title>
    <#--引入js-->
    <script src="${ctx}/js/InformManagement.js" type="text/javascript"/>
</head>
<body>
<!-- content start -->
<section class="content">
    <!-- 分类列表 -->
    <div class="row">
        <div class="col-xs-12">
            <!-- search-form start -->
            <section class="search-form hidden-print" id="search" style="display: block">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-2 col-lg-4">
                                <div class="form-group">
                                    <input id="searchTitle" name="searchGoodsId" class="form-control"
                                           style="width: 100%;" placeholder="请输入标题">
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-8">
                                <button id="queryBtn" class="btn btn-primary my-btn"><i
                                            class="fa fa-search button-in-i"></i>查询
                                </button>
                                <button id="clearBtn" class="btn btn-default my-btn"><i
                                            class="fa fa-trash button-in-i"></i>清空
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- search-form end -->
            <!-- result-table start -->
            <section class="result-table">
                <div class="box">
                    <div class="box-header table-top-button">
                        <div class="col-xs-4">
                            <!-- 新增通知按钮 -->
                            <button type="submit" class="btn btn-info my-btn" id="addNewBtn"><i
                                        class="fa fa-plus button-in-i"></i> 新增通知
                            </button>
                            <!-- 回收站按钮 -->
                            <button type="submit" class="btn btn-default my-btn" id="skipNoticeTrash"><i
                                    class="fa fa-trash button-in-i"></i> 回收站
                            </button>
                        </div>
                        <#--<div class="col-xs-1">
                            <button type="submit" class="btn btn-warning my-btn" id="trash"><i
                                        class="fa fa-plus ion-android-delete"></i> 回收站
                            </button>
                        </div>-->
                    </div>
                    <div class="grid-table">
                        <!--startprint1-->
                        <table id="grid-table" class="table-striped table-hover"></table>
                        <!--endprint1-->
                        <div id="grid-pager" class="box-footer"></div>
                    </div>
                </div>
            </section>
            <!-- result-table end -->
        </div>
    </div>
</section>
<!-- content end -->

<!--    查看  Modal -->
<div class="modal fade bs-example-modal-lg" id="showmyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="Showform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle">信息查看</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="idLable1">
                            <label for="ename" class="col-sm-2 control-label">公告ID</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="noticeId" name="noticeId" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ename" class="col-sm-2 control-label">标题</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="noteTitle" name="noteTitle" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ename" class="col-sm-2 control-label">公告内容</label>
                            <div class="col-sm-7">
                                <textarea type="text" class="form-control" id="noteContent"
                                          name="noteContent" style="width: 500px;height: 200px;" readonly/>
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-close button-in-i"></i>取消
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!--    编辑  Modal -->
<div class="modal fade bs-example-modal-lg" id="editmyModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <form id="Eidtform" class="form-horizontal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modeltitle1">###</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-8 col-xs-offset-2">
                        <div class="form-group" id="idLable">
                            <label for="ename" class="col-sm-2 control-label">公告ID</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="id" name="id" readonly placeholder="公告ID自动生成"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ename" class="col-sm-2 control-label">标题</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="name" placeholder="请输入公告标题"  name="name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ename" class="col-sm-2 control-label">公告内容</label>
                            <div class="col-sm-7">
                                <textarea type="text" class="form-control" id="content"
                                          name="content" style="width: 500px;height: 200px;" placeholder="请输入公告内容" />
                            </div>
                        </div>
                        <div class="form-group" id="result">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-close button-in-i"></i>取消
                        </button>
                        <button type="button" class="btn btn-primary" id="saveBtn"><i
                                    class="fa fa-check button-in-i"></i>保存
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>