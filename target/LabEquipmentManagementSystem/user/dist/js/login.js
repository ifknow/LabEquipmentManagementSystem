/*
* 登录验证
*/
function loginCheck() {
    if ($('#username').val() == "") {
        $("#ywMessage").html("用户名不能为空!");
        return false;
    }
    else if ($('#password').val() == "") {
        $("#ywMessage").html("密码不能为空!");
        return false;
    }
    $.post("newadmin/loginValidate", {
        "username": $('#username').val(),
        "password": $('#password').val(),
    }, function (data) {
        if (data.resultMessage == 1) {
            $("#loginform").submit();
        }
        else if (data.resultMessage == 0) {
            $("#ywMessage").html("用户名或密码错误!");
        }
    });
}
	