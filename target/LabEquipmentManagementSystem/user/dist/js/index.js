$(function () {
    //获得该机构下菜单目录列表
    getCatalogList();

    //获取登录的用户信息及权限
    var realname = localStorage.getItem("realname");
    var roleId = localStorage.getItem("roleId");
    var loginTime = localStorage.getItem("loginTime");
    if (roleId == 1) {
        $("#quanXianName").html("管理员");
    } else if (roleId == 2) {
        $("#quanXianName").html("教师");
    } else if (roleId == 3) {
        $("#quanXianName").html("学生");
    } else if (roleId == 4) {
        $("#quanXianName").html("超级管理员");
    }
    $("#loginTime").html("登录时间：" + loginTime);
    $("#loginUserName").html("姓名：" + realname);
    // alert(localStorage.getItem("roleId"))
    //超级管理员
    if (localStorage.getItem("roleId") == 4) {
        // $("#tree01").css('display', 'none');
        // $("#tree03").css('display', 'none');
        // $("#tree04").css('display', 'none');
        // $("#tree05").css('display', 'none');
        // $("#tree06").css('display', 'none');
        // $("#moudle22").css('display', 'none');
        $("#moudle32").css('display', 'none');
        $("#moudle61").css('display', 'none');
        console.log("超级管理员登陆成功...");
    }
    //分配权限
    else if (localStorage.getItem("roleId") == 1) {
        //管理员具有所有权限，不做任何操作
        $("#moudle21").css('display', 'none');
    } else {
        //教师、学生具有的权限基本相同、包括通知的查看、基本信息的查看、编辑、修改密码
        $("#moudle31").css('display', 'none');
        $("#tree04").css('display', 'none');
        $("#tree05").css('display', 'none');
        $("#moudle62").css('display', 'none');
        $("#moudle21").css('display', 'none');
        $("#moudle33").css('display', 'none');
    }


});

/*
 * 获得该机构下菜单目录列表
 */
function getCatalogList() {

    $('.sidebar .sidebar-menu > li').hover(function () {
        $('.sidebar .sidebar-menu li.treeview .hover-treeview-menu').removeClass('show');
        $('.sidebar .sidebar-menu li.treeview').removeClass('active');
        if ($(this).children('.hover-treeview-menu')) {
            $(this).children('.hover-treeview-menu').toggleClass('show');
            $(this).toggleClass('active');
        }
    });
    $('.hover-treeview-menu > li').hover(function () {
        if ($(this).children('.second-hover-treeview-menu')) {
            $(this).children('.second-hover-treeview-menu').toggleClass('show');
            $(this).toggleClass('active');
        }
    });
    $('.second-hover-treeview-menu > li').hover(function () {
        if ($(this).children('.third-hover-treeview-menu')) {
            $(this).children('.third-hover-treeview-menu').toggleClass('show');
            $(this).toggleClass('active');
        }
    });
}

/*
 * 退出登录
 */
function loginOut() {
    bootbox.confirm("您确定要将退出登录吗？", function (result) {
        if (result) {
            //1、清除localstrorage中的值
            localStorage.removeItem("userId");
            localStorage.removeItem("roleId");
            localStorage.removeItem("realname");
            localStorage.removeItem("loginTime");
            //2、跳转页面
            window.location.href = "../login.ftl";
        }
    });
}

	