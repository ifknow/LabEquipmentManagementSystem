var grid;
var form;
var form1;
$(function () {

    var pageNo = 1;
    var pageSize = 10;
    var grid_selector = "#grid-table";
    var pager_selector = "#grid-pager";

    //初始化数据表格
    grid = $(grid_selector).jqGrid({
        datatype: "json",
        url: 'users',
        mtype: 'POST',
        page: pageNo,
        postData: {
            /*'departmentId':0,*/
        },
        height: 380,
        width: 300,
        colNames: ['ID', '书名', '书号', '价格', '库存', "操作"],
        colModel: [
            {name: 'id', index: 'id', width: 50, sortable: false, align: 'center', hide: true},
            {name: 'bookName', index: 'bookName', sortable: false, align: 'center'},
            {name: 'isbn', index: 'isbn', align: 'center'},
            {name: 'price', index: 'price', align: 'center'},
            {name: 'stock', index: 'stock', align: 'center'},
            {
                name: 'action', index: '', fixed: true, sortable: false, resize: false,
                formatter: function (cellvalue, options, rowObject) {
                    var txt;
                    var shel;
                    return "<button class='btn btn-warning btn-xs' onclick='openEdit(\"" + rowObject.id + "\")'><i class='fa fa-edit button-in-i'></i>编辑</button>"
                        + "<button onclick='openDelete(\"" + rowObject.id + "\")' class='btn btn-danger btn-xs'><i class='fa fa-trash button-in-i'></i>删除</button>";
                }
            }
        ],
        viewrecords: true,
        rowNum: pageSize,
        rownumbers: false,//添加左侧行号
        rowList: [10, 20, 30],
        pager: pager_selector,
        altRows: true,
        multiselect: true,
        loadComplete: function (data) {
            console.log(data);
            var table = this;
            setTimeout(function () {
                updatePagerIcons(table);
            }, 0);
            var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
            $('.ui-jqgrid-bdiv').height(h);
        },
        autowidth: true,
        jsonReader: jsonReader,   // json中代表实际模型数据的入口
        rownumWidth: 55,
        prmNames: prmNames,
        recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
        emptyrecords: "没有记录",
        loadtext: "正在加载...",
        pgtext: "第 {0} 页  ,共 {1} 页"
    });
    //初始化分页条
    showNavGrid(grid_selector, pager_selector);
    //初始化信息校验表单
    form = $("#addform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });
    form1 = $("#Editform").validate({
        errorElement: 'div',
        errorClass: 'help-block messages',
        focusInvalid: false,
        highlight: validateHighlight,
        success: validateSuccess,
        errorPlacement: errorPlacement
    });

    $("#addBtn").click(function () {
        openAdd();
    });

    $("#addNewBtn").click(function () {
        openNewAdd();
    });

    $("#openEdit").click(function () {
        openEdit(str);
    });
    //查询
    $("#queryBtn").click(function () {
        var filters = {
            'departmentId': $("#positionName").val(),
        }
        grid.jqGrid("setGridParam", {postData: filters, page: 1});
        grid.trigger('reloadGrid');
    });
    //清空
    $("#clearBtn").click(function () {
        $("#employeeCodes").val(null).trigger("change");
        $("#warehouseCodes").val(null).trigger("change");
        $("#Qtel").val("");
        $("#empName").val("");
        $("#positionName").val(null).trigger("change");
    });

    //保存
    $("#employeeSaveBtn").click(function () {
        if (form.form()) {
            doSubmit();
        }
    });


    $("#departmentEditBtn").click(function () {
        if (form1.form()) {
            doSubmit1();
        }
    });
});

//打开新建表单(弹出框)
function openAdd() {
    clearForm();
    $("#myModal").modal();
    $("#myModeltitle").html("新建");

}

//打开新建表单(新页面)
function openNewAdd() {
    loadPage("person/goodsInfoAdd.html");
}

//打开编辑表单
function openEdit(str) {
    array = str.split(",");
    $("#editmyModal").modal();
    $("#modeltitle").html("编辑");
    $("#departmentId").val(array[0]);
    $("#department").val(array[1]);
}


//删除记录
function openDelete(str) {
    array = str.split(",");
    bootbox.confirm("您确定要将删除此行记录吗?", function (result) {
        if (result) {
            $.post("deleteDepartment", {
                "departmentId": array[0],
                "departmentName": array[1]
            }, function (data) {
                alert(data.resultMessage);
                grid.trigger('reloadGrid');
            });
        }
    });
}

//初始化权限树
function setPermission(id) {
    $("#permissionModal").modal();
    var setting = {
        view: {
            dblClickExpand: false,
            showLine: false
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        check: {
            enable: true
        }
    };

    var zNodes = [];
    $.post("getPermissionTree", {
        levelId: id
    }, function (data) {
        zNodes = eval(data.resultMessage);
        $.fn.zTree.init($("#treeDemo"), setting, zNodes);
    });

    $("#levelId").html(id);
}

//保存所选权限信息
function openPermissionSave() {
    var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
    var nodes = treeObj.getCheckedNodes(true);
    var nodelist = "";
    for (var i = 0; i < nodes.length; i++) {
        if (nodelist == "") {
            nodelist = nodes[i].id;
        } else {
            nodelist = nodelist + "," + nodes[i].id;
        }
    }
    $.post("saveLevelPermission", {
        levelId: $("#levelId").html(), permissions: nodelist
    }, function (data) {
        alert(data.resultMessage);
        $('#permissionModal').modal('hide');
    });
}

//提交表单
function doSubmit() {
    $("#loading").removeClass('hide').addClass('show');
    $.post("addDepartment", $('#addform').serialize(), function (data) {
        if (data.resultCode == 201) {
            $("#loading").removeClass('show').addClass('hide');
            alert(data.resultMessage);
        } else if (data.resultCode == 202) {
            $("#loading").removeClass('show').addClass('hide');
            alert(data.resultMessage);
        } else {
            $("#loading").removeClass('show').addClass('hide');
            alert("添加成功!");
            grid.trigger('reloadGrid');
            $('#addmyModal').modal('hide');
        }
    });
}

//提交编辑
function doSubmit1() {
    alert($('#Editform').serialize());
    $("#loading").removeClass('hide').addClass('show');
    $.post("updateDepartment", $('#Editform').serialize(), function (data) {
        alert(123);
        if (data.resultCode == 201) {
            $("#loading").removeClass('show').addClass('hide');
            alert(data.resultMessage);
        } else {
            alert("编辑成功!");
            grid.trigger('reloadGrid');
            $('#editmyModal').modal('hide');
            $("#loading").removeClass('show').addClass('hide');
        }
    });
}

//清除表单
function clearForm() {
    //清空内容
    $("#tel").val("");
    $("#ename").val("");
    $("#men").removeAttr("checked");
    $("#men1").removeAttr("checked");
    $("#women1").removeAttr("checked");
    $("#women").removeAttr("checked");
    $("#birthday").val("");
    //清除验证状态

    $("#addform .form-group").removeClass('has-info').removeClass('has-error');

}

function getPositionNames() {
    $.post("positionList", {}, function (data) {
        for (var i = 0; i < data.data.length; i++) {
            $("#positionName").append('<option  value=' + data.data[i].positionName + '>' + data.data[i].positionName + '</option>');
        }
    });
}