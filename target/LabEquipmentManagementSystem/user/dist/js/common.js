var jsonReader = {  
    root: "list",   // json中代表实际模型数据的入口  
    page: "pageNum",   // json中代表当前页码的数据  
    total: "pages", // json中代表页码总数的数据  
    records: "total", // json中代表数据行总数的数据  
    repeatitems: false, // 如果设为false，则jqGrid在解析json时，会根据name来搜索对应的数据元素（即可以json中元素可以不按顺序）；而所使用的name是来自于colModel中的name设定。    
    id: "id"
};

var prmNames = {  
    page:"pageNo",    // 表示请求页码的参数名称  
    rows:"pageSize",    // 表示请求行数的参数名称  
    sort: "sort", // 表示用于排序的列名的参数名称  
    order: "order", // 表示采用的排序方式的参数名称  
    search:"_search", // 表示是否是搜索请求的参数名称  
    nd:"nd"// 表示已经发送请求的次数的参数名称  
};

function showNavGrid(grid_selector,pager_selector){
	$(grid_selector).jqGrid('navGrid',pager_selector,
			{ 
				refresh: true,
				edit: false,
				add: false,
				del: false,
				refreshicon : 'icon-refresh green'
			}
	)
}	

function updatePagerIcons(table) {
	var replacement = 
	{
		'ui-icon-seek-first' : 'fa fa-angle-double-left',
		'ui-icon-seek-prev' : 'fa fa-angle-left',
		'ui-icon-seek-next' : 'fa fa-angle-right',
		'ui-icon-seek-end' : 'fa fa-angle-double-right'
	};
	var replacement2 = {
		'ui-icon-search' : 'hide',
		'icon-refresh green' : 'fa fa-refresh text-green'
	};
	$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
		var icon = $(this);
		var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
		if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
	})
	$('.ui-pg-table.navtable > tbody > tr > .ui-pg-button > .ui-pg-div > .ui-icon').each(function(){
		var icon2 = $(this);
		var $class2 = $.trim(icon2.attr('class').replace('ui-icon', ''));
		if($class2 in replacement2) icon2.attr('class', 'ui-icon '+replacement2[$class2]);
	})
}

function validateHighlight(e){
	$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
}

function validateSuccess(e){
	$(e).closest('.form-group').removeClass('has-error').addClass('has-info');
	$(e).remove();
}

function errorPlacement (error, element) {
	if(element.is(':checkbox') || element.is(':radio')) {
		var controls = element.closest('div[class*="col-"]');
		if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
		else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
	}
	else if(element.is('.select2')) {
		error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
	}
	else if(element.is('.chosen-select')) {
		error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
	}
	else error.insertAfter(element.parent());
}

function loadPage(url) {
	$('.content-wrapper').html("");//先清空
	$('.content-wrapper').load(url);
}


//扩展Date的format方法
Date.prototype.format = function(format) {
	var o = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"h+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S" : this.getMilliseconds()
	}
	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}
	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
					: ("00" + o[k]).substr(("" + o[k]).length));
		}
	}
	return format;
}

$(function(){
	
    //Initialize Select2 Elements
	$(".select2").select2();
	
	//Date picker
    $('#datepicker').datepicker({ 
    	clearBtn: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd",//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
        language: "zh-CN"
    });

    //Date range picker
    $('#reservation').daterangepicker({
        autoUpdateInput: false,
        locale: {
            applyLabel: '确定',
            cancelLabel: '清除',
            format: 'YYYY/MM/DD'
        }
    });
    $('#reservation').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
    });
    $('#reservation').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });


		var grid;
		var pageNo="${pageNo!}";
		var pageSize="${pageSize!}";
		var grid_selector = "#grid-table";
		var pager_selector = "#grid-pager";
	
		//初始化数据表格
		grid = $(grid_selector).jqGrid({
			datatype: "json",
			url:'../admin/goodsKuPage',
			mtype: 'POST',
			page: 1,
			postData:{isAdmin:"1"},
			height: 'auto',
			colNames:[ '商品类目','商品ID','商品名称','商品类别','缩略图','是否审核','是否推荐','添加时间',' 操作'],
			colModel:[
			    {name:'categoryId',index:'categoryId', width:50, sortable:false, align:'center'},     
                {name:'id',index:'id', width:50, sortable:false},      
				{name:'goodsName',index:'goodsName', sortable:false},
				{name:'className',index:'className',width:70},
				{name:'goodsImg',index:'goodsImg', width:80, sortable:false,formatter:function(cellvalue, options, rowObject){
					return '<img src="'+cellvalue+'" style="height:60px;">';//待处理
				  }
			    },
			    {name:'isAudit',index:'isAudit', width:50, sortable:false,formatter:function(cellvalue, options, rowObject){
					if(cellvalue=='1'){
						return '<span class="label label-success">已通过</span>';
					}else if(cellvalue=='0'){
						return '<span class="label label-info"><s>待审核</s></span>';
					}else if(cellvalue=='-1'){
						return '<span class="label label-default"><s>未通过</s></span>';
					}else{
						return '<span class="label label-warning">未知</span>';
					 }
				   }
			     },
				{name:'recommend',index:'recommend', width:50, sortable:false,formatter:function(cellvalue, options, rowObject){
						if(cellvalue=='1'){
							return '<span class="label label-success">推荐</span>';
						}else if(cellvalue=='0'){
							return '<span class="label label-default"><s>未推荐</s></span>';
						}
					   }
				 },
				{name:'addTime',index:'addTime', width:100, sorttype:"date",formatter:function(cellvalue, options, rowObject){
					if(cellvalue){
						return new Date(cellvalue).format("yyyy-MM-dd hh:mm:ss");
					}else{
						return "";
					}
				}} ,
				{name:'action',index:'', width:200, fixed:true, sortable:false, resize:false,
					formatter:function(cellvalue, options, rowObject){
						var txt;
						var shel;
					/* 	if('0'==rowObject.state)
							txt="解禁";
						else 
							txt="禁用"; */
					
						return "<button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#myModal'><i class='fa fa-edit button-in-i'></i>编辑</button>"
						+"<button onclick='openDelete(\""+rowObject.id+"\")' class='btn btn-danger btn-xs'><i class='fa fa-trash button-in-i'></i>删除</button>"
					    +"<button class='btn btn-primary btn-xs' data-toggle='modal' data-target='#myModal2'><i class='fa fa-gear button-in-i'></i>规格管理</button>";
					}
				}
			], 
			viewrecords : true,
			rowNum:10, 
			rownumbers:false,//添加左侧行号
			rowList:[10,20,30],
			pager : pager_selector,
			altRows: true,
			sortable: true,
			//toppager: true,
			multiselect: true,
			//multikey: "ctrlKey",
	        //multiboxonly: true,
			loadComplete : function() {
				var table = this;
				setTimeout(function(){
					updatePagerIcons(table);
				}, 0);
				var h = $(window).height() - ($('#grid-pager')[0].offsetHeight + 15) - ($('.result-table .box-header')).outerHeight(true) - ($('.result-table .box')[0].offsetTop + $('.content .col-xs-12')[0].offsetTop + $('.ui-jqgrid-bdiv')[0].offsetTop + 3);
				$('.ui-jqgrid-bdiv').height(h);	
				
			},
			autowidth: true,
			jsonReader : jsonReader,
			prmNames : prmNames,
			recordtext: "显示 {0} - {1} 条 , 共 {2} 条",
			emptyrecords: "没有记录",
			loadtext: "正在加载...",
			pgtext : "当前第 {0} 页  ,共 {1} 页"
		});
		//初始化分页条
		showNavGrid(grid_selector,pager_selector);

		$(window).resize(function(){   
	        $('#grid-table').setGridWidth($('.grid-table').width());
	    });
		
		$('.sidebar-toggle').click(function(){
			var w = 0;
			if($('body').hasClass('sidebar-collapse')){
				w = $(window).width() - 260;
			}else{
				w = $(window).width() - 80;
			}
			$('#grid-table').setGridWidth(w);
		});
		
	    //下拉多选框
	    $('[data-toggle="select-checkbox-dropdown"]').click(function(){
	    	$(this).parent('.multi-checkbox').toggleClass('open');
	    });
	    $('.multi-checkbox').hover(
	    	function(){},
	    	function(){
	    		$(this).removeClass('open');
	    	}
	    );
	    /**
	     * 点击隐藏列、显示列
	     */
		$('#showHiddenColumns li').click(function(){
			var colName = $(this).attr('data-colname');
			var len = $(grid_selector).getGridParam("width");
			if($(this).children('i').hasClass('fa-check-square-o')){
				$(this).children('i').removeClass('fa-check-square-o').addClass('fa-square-o');
				$(grid_selector).setGridParam().hideCol(colName).trigger("reloadGrid");
			}else{
				$(this).children('i').removeClass('fa-square-o').addClass('fa-check-square-o');
				$(grid_selector).setGridParam().showCol(colName).trigger("reloadGrid");
			}
			$(grid_selector).setGridWidth(len);
		});
});

//上传图片，直接显示缩略图
function setImagePreview(fileInput, imgPreview, imgDiv) { 
	var docObj=document.getElementById(fileInput); 
  	var imgObjPreview=document.getElementById(imgPreview); 
	var localImagId = document.getElementById(imgDiv); 
  	if(docObj.files && docObj.files[0]){ 
  		//火狐下，直接设img属性 
  		localImagId.style.display = 'block'; 
  		imgObjPreview.style.width = '100px'; 
  		imgObjPreview.style.height = '100px'; 
  		//imgObjPreview.src = docObj.files[0].getAsDataURL(); 
  		//火狐7以上版本不能用上面的getAsDataURL()方式获取，需要一下方式 
  		imgObjPreview.src = window.URL.createObjectURL(docObj.files[0]); 
  	}else{ 
  		//IE下，使用滤镜 
  		docObj.select(); 
  		var imgSrc = document.selection.createRange().text; 
  		//必须设置初始大小 
  		localImagId.style.width = "100px"; 
  		localImagId.style.height = "100px"; 
  		//图片异常的捕捉，防止用户修改后缀来伪造图片 
  		try{ 
	  		localImagId.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
	  		localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc; 
  		}catch(e){ 
  			alert("您上传的图片格式不正确，请重新选择!"); 
  			return false; 
  		} 
  		localImagId.style.display = 'none'; 
  		document.selection.empty(); 
  	} 
  	return true; 
} 

function printdiv(printDiv){  
    var oPop = window.open('','oPop');
	var printstr = $(printDiv)[0].innerHTML;  
    var str = '<!DOCTYPE html>';  
        str +='<html>';  
        str +='<head>';  
        str +='<meta charset="utf-8">';  
        str +='<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';  
        str +='<style>'; 
        str +='* {font-family: "microsoft yahei";}';
        str +='span.ui-jqgrid-resize.ui-jqgrid-resize-ltr {display: none;}';
        str +='#load_grid-table{display: none!important;}';
        str +='.ui-jqgrid-bdiv {overflow: auto;}';
        str +='#grid-table th, #grid-table td {padding: 8px 0;text-align: center;}';
        str +='tr.jqgfirstrow td {padding: 1px!important;}';
        str +='.ui-icon{ display: inline-block; padding: 0; width: 24px; height: 24px; line-height: 22px; text-align: center; position: static; float: none; margin: 0 2px !important; color: #808080; border: 1px solid #CCC; background-color: #FFF; border-radius: 100%; }';
        str +='.ui-state-disabled { opacity: .35;　filter: Alpha(Opacity=35);　background-image: none;　}';
        str +='div#rs_mgrid-table {　height: 0;}';
        str +='input.ui-pg-input {　text-align: center;　}';
        str +='div#alertmod_grid-table {　display: none;　}';
        str +='.label-success { background-color: #5cb85c; }';
        str +='.bg-green, .callout.callout-success, .alert-success, .label-success, .modal-success .modal-body { background-color: #00a65a !important; }';
        str +='.bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green, .bg-navy, .bg-teal, .bg-olive, .bg-lime, .bg-orange, .bg-fuchsia, .bg-purple, .bg-maroon, .bg-black, .bg-red-active, .bg-yellow-active, .bg-aqua-active, .bg-blue-active, .bg-light-blue-active, .bg-green-active, .bg-navy-active, .bg-teal-active, .bg-olive-active, .bg-lime-active, .bg-orange-active, .bg-fuchsia-active, .bg-purple-active, .bg-maroon-active, .bg-black-active, .callout.callout-danger, .callout.callout-warning, .callout.callout-info, .callout.callout-success, .alert-success, .alert-danger, .alert-error, .alert-warning, .alert-info, .label-danger, .label-info, .label-warning, .label-primary, .label-success, .modal-primary .modal-body, .modal-primary .modal-header, .modal-primary .modal-footer, .modal-warning .modal-body, .modal-warning .modal-header, .modal-warning .modal-footer, .modal-info .modal-body, .modal-info .modal-header, .modal-info .modal-footer, .modal-success .modal-body, .modal-success .modal-header, .modal-success .modal-footer, .modal-danger .modal-body, .modal-danger .modal-header, .modal-danger .modal-footer { color: #fff !important; }';
        str +='.label { display: inline; padding: .2em .6em .3em; font-size: 75%; font-weight: 700; line-height: 1; color: #fff; text-align: center; white-space: nowrap; vertical-align: baseline; border-radius: .25em; }';
        str +='.label-default { background-color: #d2d6de; color: #444; }';
        str +='.bg-yellow, .callout.callout-warning, .alert-warning, .label-warning, .modal-warning .modal-body { background-color: #f39c12 !important; }';
        str +='.bg-aqua, .callout.callout-info, .alert-info, .label-info, .modal-info .modal-body { background-color: #00c0ef !important; }';
        str +='</style>';
        str +='</head>'; 
        str +='<body>';  
        str +=printstr;  
        str +='</body>';  
        str +='</html>';
    oPop.document.write(str); 
    oPop.print();  
    oPop.close();  
}

function loadCss(src,fn){
    var node=document.createElement('link');
    node.rel='stylesheet';
    node.href=src;
    document.head.insertBefore(node,document.head.firstChild);
    if(node.attachEvent){
        node.attachEvent('onload', function(){fn(null,node)});
    }else{
       setTimeout(function() {
         poll(node, fn);
       }, 0); // for cache
    }
    function poll(node,callback){
        var isLoaded = false;
        if(/webkit/i.test(navigator.userAgent)) {//webkit
            if (node['sheet']) {
                isLoaded = true;
              }
        }else if(node['sheet']){// for Firefox
            try{
                if (node['sheet'].cssRules) {
                      isLoaded = true;
                }
              }catch(ex){
                // NS_ERROR_DOM_SECURITY_ERR
                if (ex.code === 1000) {
                     isLoaded = true;
                }
            }
        }
        if(isLoaded){
            setTimeout(function(){
                callback(null,node);
            },1);
        }else{
            setTimeout(function(){
                poll(node,callback);
            },10);
        }
    }
    node.onLoad=function(){
        fn(null,node);
    }
} 

/**
 * jqgrid 导出数据
 * 方法一
 **/
//jqgrid.excel.js
function getXlsFromTbl1(inTblId){
	$('#'+inTblId).jqgridToExcel('测试', '2017-1-3', null, true, '测试.xls');
}

/**
 * jqgrid 导出数据
 * 方法二
 **/
function getXlsFromTbl(inTblId, inTblContainerId, title, rownumbers) {  
    try {  
        var allStr = "";  
        var curStr = "";  
        //alert("getXlsFromTbl");  
        if (inTblId != null && inTblId != "" && inTblId != "null") {  
            curStr = getTblData($('#' + inTblId), $('#' + inTblContainerId), rownumbers);  
        }  
        if (curStr != null) {  
            allStr += curStr;  
        }  
        else {  
            alert("你要导出的表不存在！");  
            return;  
        }  
        var fileName = getExcelFileName(title);  
        doFileExport(fileName, allStr);  
    }  
    catch (e) {  
        alert("导出发生异常:" + e.name + "->" + e.description + "!");  
    }  
}  
function getTblData(curTbl, curTblContainer, rownumbers) {  
  
    var outStr = "";  
    if (curTbl != null) {  
        var rowdata = curTbl.getRowData();  
        var Lenr = 1;  
  
        for (i = 0; i < Lenr; i++) {  
            //var Lenc = curTbl.rows(i).cells.length;  
            var th;  
            if (rownumbers == true) {  
                th = curTblContainer.find('TH:not(:first-child)');  
            }  
            else {  
                th = curTblContainer.find('TH');  
            }  
            th.each(function(index, element) {  
                //alert($(element).text());  
                //取得每行的列数  
                var j = index + 1;  
                var content = $(element).text();  
                //alert(j + "|" + content);  
                outStr += content + "\t";  
                //赋值  
  
            });  
            outStr += "\r\n";  
        }  
        var tmp = "";  
        for (i = 0; i < rowdata.length; i++) {  
            var row = eval(rowdata[i]);  
            for (each in row) {  
                outStr += row[each] + "\t";  
            }  
            outStr += "\r\n";  
        }  
  
  
    }  
    else {  
        outStr = null;  
        alert(inTbl + "不存在!");  
    }  
    return outStr;  
}  
function getExcelFileName(title) {  
    var d = new Date();  
    var curYear = d.getYear();  
    var curMonth = "" + (d.getMonth() + 1);  
    var curDate = "" + d.getDate();  
    var curHour = "" + d.getHours();  
    var curMinute = "" + d.getMinutes();  
    var curSecond = "" + d.getSeconds();  
    if (curMonth.length == 1) {  
        curMonth = "0" + curMonth;  
    }  
    if (curDate.length == 1) {  
        curDate = "0" + curDate;  
    }  
    if (curHour.length == 1) {  
        curHour = "0" + curHour;  
    }  
    if (curMinute.length == 1) {  
        curMinute = "0" + curMinute;  
    }  
    if (curSecond.length == 1) {  
        curSecond = "0" + curSecond;  
    }  
    var fileName = title + "_" + curYear + curMonth + curDate + "_"  
            + curHour + curMinute + curSecond + ".csv";  
    //alert(fileName);   
    return fileName;  
}  
function doFileExport(inName, inStr) {  
    var xlsWin = null;  
    if (!!document.all("HideFrm")) {  
        xlsWin = HideFrm;  
    }  
    else {  
        var width = 6;  
        var height = 4;  
        var openPara = "left=" + (window.screen.width / 2 - width / 2)  
                + ",top=" + (window.screen.height / 2 - height / 2)  
                + ",scrollbars=no,width=" + width + ",height=" + height;  
        xlsWin = window.open("", "_blank", openPara);  
    }  
    xlsWin.document.write(inStr);  
    xlsWin.document.close();  
    xlsWin.document.execCommand('SaveAs', true, inName);
    xlsWin.close();  
}   