<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>实验室设备管理系统注册页面</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="user/dist/img/favicon.ico" rel="shortcut icon"/>
    <link rel="stylesheet" href="user/bootstrap/css/bootstrap.min.css">
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
    <link rel="stylesheet" href="user/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="user/plugins/iCheck/square/blue.css">
    <!--    <link rel="stylesheet" href="assets/css/font-awesome.min.css"/>-->
    <link rel="stylesheet" href="user/dist/css/common.css">
    <!-- 百度地图 -->
    <!-- <script type="text/javascript"
             src="https://api.map.baidu.com/api?v=3.0&ak=GFiYNQ9adSaq6TeCHRFeiWGqV8Ykww6k"></script>-->
    <!-- 高德地图 &ndash;&gt;
    <script type="text/javascript"
            src="http://webapi.amap.com/maps?v=1.3&key=1d04fb280460062c676179ed18fb1f5a"></script>-->
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="user/dist/img/favicon.ico" rel="shortcut icon"/>
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="user/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!--    <link rel="stylesheet" href="user/plugins/fontawesome/css/font-awesome.min.css">-->
    <!-- Ionicons -->
    <!--    <link rel="stylesheet" href="user/plugins/ionicons/css/ionicons.min.css">-->
    <!-- jvectormap -->
    <link rel="stylesheet" href="user/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="user/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="user/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="user/plugins/daterangepicker/daterangepicker.css">
    <script src="user/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="assets/js/jquery-ui-1.10.3.full.min.js"></script>
    <script src="assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/jquery.sparkline.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/date-time/moment.min.js"></script>
    <script src="assets/js/flot/jquery.flot.min.js"></script>
    <script src="assets/js/flot/jquery.flot.pie.min.js"></script>
    <script src="assets/js/flot/jquery.flot.resize.min.js"></script>
    <script src="assets/js/chosen.jquery.min.js"></script>
    <script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
    <script src="assets/js/jqGrid/jquery.jqGrid.min.js"></script>
    <script src="assets/js/jqGrid/i18n/grid.locale-en.js"></script>
    <script src="assets/js/bootbox.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="user/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="user/dist/js/app.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="user/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- Select2 -->
    <script src="user/plugins/select2/select2.full.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="user/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- date-range-picker -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
    <script src="user/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datetimepicker -->
    <script src="user/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="user/plugins/datetimepicker/bootstrap-datetimepicker.zh-CN.js"></script>
</head>
<body class="hold-transition login-page">
<div id="video_cover" style="width: 100%;height: 800px"></div>
<div id="overlay"></div>
<div class="login-box">
    <div class="register-box-body">
        <p class="register-box-msg">实验室设备管理系统&nbsp;&nbsp;<b>注册</b>&nbsp;&nbsp;页面</p>
        <form id="loginform" action="newadmin/index.html">
            <div class="form-group has-feedback">
                <input id="userAccount" type="text" class="form-control" placeholder="请输入手机号">
            </div>
            <div class="form-group has-feedback">
                <input id="realName" type="text" class="form-control" placeholder="请输入真实姓名">
            </div>
            <div class="form-group has-feedback">
                <input id="xy" type="text" class="form-control" placeholder="请输入您所在学院">
            </div>
            <div class="form-group has-feedback">
                <input id="bj" type="text" class="form-control" placeholder="请输入您所在班级">
            </div>
            <div class="form-group has-feedback">
                <input id="number" type="text" class="form-control" placeholder="请输入您学号/工号">
            </div>
            <div class="form-group has-feedback">
                <input id="address" type="text" class="form-control" placeholder="请输入您常住地址">
            </div>
            <div class="form-group has-feedback">
                <input id="userPassword" type="password" class="form-control" placeholder="请输入密码">
            </div>
            <div class="form-group has-feedback">
                <input id="comfirmUserPassword" type="password" class="form-control" placeholder="请确认密码">
            </div>
            <div class="form-group has-feedback">
                <select id="roleId" name="roleId" class="form-control select2"
                        style="width: 100%;" data-placeholder="--/ 请选择角色 /--">
                    <option value="0">--/ 请选择角色 /--</option>
                    <option value="3"> 学生</option>
                    <option value="2"> 教师</option>
                </select>
                <input hidden id="validateMessage" value=""/>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <span id="Message"
                                  style="color:#1a11ff;text-align:center;display: inline-block;width: 100%; padding-left: 10px;font-weight: bold"></span>
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="button" class="btn btn-primary btn-block btn-flat" onclick="register()">
                        <i class="fa icon-key button-in-i"></i>注册
                    </button>
                </div>
            </div>
            <div class="row">
                <span id="ywMessage" style="color:red;text-align:center;display: inline-block;width: 100%;"></span>
                <div class="col-xs-6" style="float: right;">
                    <a href="login.ftl" style=" text-decoration: none;">已有账号，点这里登录</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="user/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="user/bootstrap/js/bootstrap.min.js"></script>
<script src="user/plugins/iCheck/icheck.min.js"></script>
<script src="js/register.js"></script>
</body>
</html>
